#LyX 2.2 created this file. For more info see http://www.lyx.org/
\lyxformat 508
\begin_document
\begin_header
\save_transient_properties true
\origin unavailable
\textclass beamer
\begin_preamble
% We use the "Berkeley" theme with a 3.45em-wide side bar on the left
\usetheme[left,width=3.45em]{Goettingen}

\usepackage{caption}

\setbeamertemplate{blocks}[shadow=true]
\setbeamercolor{block title}{bg=blue!30,fg=black}
\renewcommand\makebeamertitle{\frame[label=mytitle]{\maketitle}}
\usepackage{tikz}
\end_preamble
\use_default_options true
\maintain_unincluded_children false
\language english
\language_package default
\inputencoding auto
\fontencoding global
\font_roman "default" "default"
\font_sans "default" "default"
\font_typewriter "default" "default"
\font_math "auto" "auto"
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100 100
\font_tt_scale 100 100
\graphics default
\default_output_format pdf2
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing single
\use_hyperref false
\papersize default
\use_geometry true
\use_package amsmath 1
\use_package amssymb 1
\use_package cancel 0
\use_package esint 1
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 1
\use_package stackrel 0
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine natbib
\cite_engine_type numerical
\biblio_style humannat
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 1
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\quotes_language english
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Title
tACS stimulation in conflict task
\begin_inset VSpace medskip
\end_inset


\end_layout

\begin_layout Subtitle
PhD Retreat Neuroscience
\end_layout

\begin_layout Author
Albert Lehr
\end_layout

\begin_layout Date
20.11.
 2017
\end_layout

\begin_layout TitleGraphic
\begin_inset Graphics
	filename /home/albert/Desktop/Lyxfiles/Pictures_for_Lyx/First_Thesis_report/GPNeuro.jpg
	lyxscale 30
	scale 25

\end_inset


\begin_inset Graphics
	filename /home/albert/Desktop/Lyxfiles/Pictures_for_Lyx/First_Thesis_report/UMG.jpg
	lyxscale 10
	scale 10

\end_inset


\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
AtBeginSubsection[]{
\end_layout

\begin_layout Plain Layout

\end_layout

\begin_layout Plain Layout

  
\backslash
frame<beamer>{ 
\end_layout

\begin_layout Plain Layout

\end_layout

\begin_layout Plain Layout

    %
\backslash
frametitle{Outline}   
\end_layout

\begin_layout Plain Layout

\end_layout

\begin_layout Plain Layout

    
\backslash
tableofcontents[currentsection,currentsubsection] 
\end_layout

\begin_layout Plain Layout

\end_layout

\begin_layout Plain Layout

  }
\end_layout

\begin_layout Plain Layout

}
\end_layout

\begin_layout Plain Layout

\end_layout

\begin_layout Plain Layout

\end_layout

\begin_layout Plain Layout

% to make Beamer-handout with notes: save this file as *_handout.lyx, set
 Document => Settings => Document class(on left drop down menu) => costum
 => insert * inside "*"=> "pt=12, handout, notes=show" 
\end_layout

\end_inset


\end_layout

\begin_layout Frame
\begin_inset Separator latexpar
\end_inset


\end_layout

\begin_deeper
\begin_layout FrameTitle
\begin_inset Argument 1
status open

\begin_layout Plain Layout
presentation
\end_layout

\end_inset

Contents
\end_layout

\begin_layout Standard
\begin_inset CommandInset toc
LatexCommand tableofcontents

\end_inset


\end_layout

\end_deeper
\begin_layout Section
Introduction
\end_layout

\begin_layout Subsection
Electrical Stimulation
\end_layout

\begin_layout Frame
\begin_inset Argument 2
status open

\begin_layout Plain Layout
+-
\end_layout

\end_inset


\begin_inset Argument 4
status open

\begin_layout Plain Layout
Low-intensity transcranial Electrical Stimulation
\end_layout

\end_inset


\end_layout

\begin_layout Frame
Developed as direct current stimulation (tDCS) in Göttingen in 2000 
\begin_inset CommandInset citation
LatexCommand citep
key "Nitsche2000"

\end_inset


\end_layout

\begin_layout Frame
\begin_inset Separator latexpar
\end_inset


\end_layout

\begin_deeper
\begin_layout Pause

\end_layout

\begin_layout Itemize
induction of online and after-effects
\end_layout

\begin_layout Itemize
non-invasive, painless (
\begin_inset Formula $\leq$
\end_inset


\begin_inset space ~
\end_inset

4
\begin_inset space ~
\end_inset

mA)
\end_layout

\begin_layout Itemize
easy applicable and portable
\end_layout

\begin_layout Itemize
affordable
\end_layout

\begin_layout Itemize
Probable efficacy for fibromyalgia, MD episode without drug resistance,
 addiction and craving 
\begin_inset CommandInset citation
LatexCommand citep
key "Lefaucheur2017"

\end_inset


\end_layout

\begin_layout Itemize
\begin_inset Flex Alert
status open

\begin_layout Plain Layout
Only used for cortical brain structures
\end_layout

\end_inset


\end_layout

\begin_layout Itemize
\begin_inset Flex Alert
status open

\begin_layout Plain Layout
effective targeting 
\end_layout

\end_inset


\end_layout

\end_deeper
\begin_layout Standard
\begin_inset Separator parbreak
\end_inset


\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
note[itemize]{
\end_layout

\begin_layout Plain Layout


\backslash
item level B recommendation "Probable efficacy" // Fibromyalgia: heightened
 pain response to pressure
\end_layout

\begin_layout Plain Layout


\backslash
item We can go up to 4 mA, but usually we are at 1 - 2 mA.
 It does not hurt, but it's perceptable
\end_layout

\begin_layout Plain Layout

}
\end_layout

\end_inset


\end_layout

\begin_layout Frame
\begin_inset Argument 2
status collapsed

\begin_layout Plain Layout
+-
\end_layout

\end_inset


\begin_inset Argument 4
status open

\begin_layout Plain Layout
Alternating and Random Noise Stimulation 
\end_layout

\end_inset


\end_layout

\begin_layout Frame
\begin_inset Separator latexpar
\end_inset


\end_layout

\begin_deeper
\begin_layout Overprint
\begin_inset Argument item:1
status open

\begin_layout Plain Layout
1-4|handout:1
\end_layout

\end_inset


\begin_inset Separator latexpar
\end_inset


\end_layout

\begin_deeper
\begin_layout Block
\begin_inset Argument 2
status open

\begin_layout Plain Layout
transcranial Alternating Current Stimulation (tACS)
\end_layout

\end_inset


\end_layout

\begin_layout Itemize
\begin_inset Argument item:2
status open

\begin_layout Plain Layout
2-4
\end_layout

\end_inset

Entrainment of physiological neural oscillations
\end_layout

\begin_layout Itemize
\begin_inset Argument item:2
status open

\begin_layout Plain Layout
3-4
\end_layout

\end_inset

pertubation of oscillations alter cognitive functions and allow to investigate
 oscillations and neural networks 
\begin_inset CommandInset citation
LatexCommand citep
key "Chander2016"

\end_inset

 
\end_layout

\begin_layout Itemize
\begin_inset Argument item:2
status open

\begin_layout Plain Layout
4-4
\end_layout

\end_inset

Nesting of frequencies, e.g.
 Theta-Gamma coupling 
\begin_inset CommandInset citation
LatexCommand citep
key "Alekseichuk2016a"

\end_inset


\end_layout

\end_deeper
\begin_layout Overprint
\begin_inset Argument item:1
status open

\begin_layout Plain Layout
5-|handout:2
\end_layout

\end_inset


\begin_inset Separator latexpar
\end_inset


\end_layout

\begin_deeper
\begin_layout Block
\begin_inset Argument 2
status open

\begin_layout Plain Layout
transcranial Random Noise Stimulation (tRNS)
\end_layout

\end_inset


\end_layout

\begin_layout Itemize
\begin_inset Argument item:2
status open

\begin_layout Plain Layout
6-
\end_layout

\end_inset

application of alternating currents with randomly distributed frequencies
\end_layout

\begin_layout Itemize
\begin_inset Argument item:2
status open

\begin_layout Plain Layout
7-
\end_layout

\end_inset

Increase of detectability of sub-threshold signals in perception task 
\begin_inset CommandInset citation
LatexCommand citep
key "VanderGroen2016"

\end_inset

 
\end_layout

\begin_layout Itemize
\begin_inset Argument item:2
status open

\begin_layout Plain Layout
8-
\end_layout

\end_inset

 behavioral improvements in arithmetic task for up to 6 months 
\begin_inset CommandInset citation
LatexCommand citep
key "Snowball2013"

\end_inset


\end_layout

\begin_layout Itemize
\begin_inset Argument item:2
status open

\begin_layout Plain Layout
9-
\end_layout

\end_inset

 No phosphenes induced, less skin perception, mechanism unkown
\end_layout

\end_deeper
\end_deeper
\begin_layout Standard
\begin_inset Separator parbreak
\end_inset


\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
note[itemize]{
\end_layout

\begin_layout Plain Layout


\backslash
item stochasitic resonance => look for paper where they increased cognitive
 function with tRNS in young adults 
\end_layout

\begin_layout Plain Layout


\backslash
item tRNS: we only no that blocking NMDA receptors doesnt abolish the effect,
 but blocking sodium (Na+) channels delays the effects of tRNS
\end_layout

\begin_layout Plain Layout

}
\end_layout

\end_inset


\end_layout

\begin_layout Subsection
Stroop Task
\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Stroop Task
\end_layout

\end_inset


\end_layout

\begin_layout Frame
Discovered 1935 
\begin_inset CommandInset citation
LatexCommand citep
key "Stroop1935"

\end_inset

, naming ink color of incongruent words (
\color green
RED
\color inherit
) slower than congruent words (
\color red
RED
\color inherit
), conflict task
\end_layout

\begin_layout Frame
\begin_inset Separator latexpar
\end_inset


\end_layout

\begin_deeper
\begin_layout Overprint
\begin_inset Argument item:1
status open

\begin_layout Plain Layout
1-1|handout:1
\end_layout

\end_inset

 
\begin_inset Separator latexpar
\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status collapsed

\begin_layout Plain Layout
\begin_inset Graphics
	filename /home/albert/Desktop/Lyxfiles/Pictures_for_Lyx/First_Thesis_report/Stroop_effect_Hanslmayr_2008.png
	lyxscale 30
	scale 20

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption Standard

\begin_layout Plain Layout
adapted from 
\begin_inset CommandInset citation
LatexCommand citealt
key "Hanslmayr2008"

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout

\end_layout

\end_inset


\end_layout

\end_deeper
\begin_layout Overprint
\begin_inset Argument item:1
status open

\begin_layout Plain Layout
2-7|handout:2
\end_layout

\end_inset


\begin_inset Separator latexpar
\end_inset


\end_layout

\begin_deeper
\begin_layout Block
\begin_inset Argument 2
status open

\begin_layout Plain Layout
functional Magnetic Resonance Imaging (fMRI)
\end_layout

\end_inset


\end_layout

\begin_layout Itemize
\begin_inset Argument item:2
status open

\begin_layout Plain Layout
3-7
\end_layout

\end_inset

co-activation of left DLPFC and dACC 
\begin_inset CommandInset citation
LatexCommand citep
key "Miller2001"

\end_inset


\end_layout

\begin_layout Itemize
\begin_inset Argument item:2
status open

\begin_layout Plain Layout
4-7
\end_layout

\end_inset

 dACC/ LDLPFC activation correlated with interference effect 
\begin_inset CommandInset citation
LatexCommand citep
key "Floden2011"

\end_inset


\end_layout

\begin_layout Itemize
\begin_inset Argument item:2
status open

\begin_layout Plain Layout
5-7
\end_layout

\end_inset

LDLPFC activated before dACC in cascade-of-control 
\begin_inset CommandInset citation
LatexCommand citep
key "Silton2010,Banich2009"

\end_inset


\end_layout

\begin_layout Itemize
\begin_inset Argument item:2
status open

\begin_layout Plain Layout
6-7
\end_layout

\end_inset

 dACC not involved in interference effect 
\begin_inset CommandInset citation
LatexCommand citep
key "Zysset"

\end_inset

, rather response-related processes 
\begin_inset CommandInset citation
LatexCommand citep
key "Liu2006"

\end_inset


\end_layout

\begin_layout Itemize
\begin_inset Argument item:2
status open

\begin_layout Plain Layout
7-7
\end_layout

\end_inset

dACC activity is correlated to response times and inversely with error rate
 
\begin_inset CommandInset citation
LatexCommand citep
key "Silton2010"

\end_inset

 
\end_layout

\end_deeper
\begin_layout Overprint
\begin_inset Argument item:1
status open

\begin_layout Plain Layout
8-11|handout:3
\end_layout

\end_inset


\begin_inset Separator latexpar
\end_inset


\end_layout

\begin_deeper
\begin_layout Block
\begin_inset Argument 2
status open

\begin_layout Plain Layout
Event-related potential (ERP)
\end_layout

\end_inset


\end_layout

\begin_layout Itemize
\begin_inset Argument item:2
status open

\begin_layout Plain Layout
9-11
\end_layout

\end_inset

 Stroop interference occurs after 300
\begin_inset space ~
\end_inset

ms as P300 unchanged by interference 
\begin_inset CommandInset citation
LatexCommand citep
key "Duncan-Johnson1981"

\end_inset


\end_layout

\begin_layout Itemize
\begin_inset Argument item:2
status open

\begin_layout Plain Layout
10-11
\end_layout

\end_inset

ACC activated 350
\begin_inset space ~
\end_inset

-
\begin_inset space ~
\end_inset

500
\begin_inset space ~
\end_inset

ms post-stimulus 
\begin_inset CommandInset citation
LatexCommand citep
key "Liotti2000"

\end_inset


\end_layout

\begin_layout Itemize
\begin_inset Argument item:2
status open

\begin_layout Plain Layout
11-11
\end_layout

\end_inset

Theta band power (4
\begin_inset space ~
\end_inset

-
\begin_inset space ~
\end_inset

7
\begin_inset space ~
\end_inset

Hz) and ERP component similarly modulated by conflict task 
\begin_inset CommandInset citation
LatexCommand citep
key "Cohen2011"

\end_inset

, ACC estimated to be strongest conflict-related generator of fronto-medial
 Theta 
\begin_inset CommandInset citation
LatexCommand citep
key "Kovacevic2012"

\end_inset


\end_layout

\end_deeper
\begin_layout Overprint
\begin_inset Argument item:1
status open

\begin_layout Plain Layout
12-14|handout:4
\end_layout

\end_inset


\begin_inset Separator latexpar
\end_inset


\end_layout

\begin_deeper
\begin_layout Block
\begin_inset Argument 2
status open

\begin_layout Plain Layout
BESA dipole source analysis
\end_layout

\end_inset


\end_layout

\begin_layout Itemize
\begin_inset Argument item:2
status open

\begin_layout Plain Layout
13-14
\end_layout

\end_inset

Fronto-central negative ERP at 400
\begin_inset space ~
\end_inset

ms more pronounced for incongruence 
\begin_inset CommandInset citation
LatexCommand citep
key "Hanslmayr2008"

\end_inset


\begin_inset Separator latexpar
\end_inset


\end_layout

\begin_deeper
\begin_layout Itemize
\begin_inset Argument item:2
status open

\begin_layout Plain Layout
14-
\end_layout

\end_inset

ACC as source
\end_layout

\begin_layout Itemize
\begin_inset Argument item:2
status open

\begin_layout Plain Layout
14-
\end_layout

\end_inset

theta oscillation power in ACC linearly correlated to interference
\end_layout

\end_deeper
\end_deeper
\end_deeper
\begin_layout Standard
\begin_inset Separator parbreak
\end_inset


\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
note[itemize]{
\end_layout

\begin_layout Plain Layout


\backslash
item why do we use a stroop task? because it has been used before and serves
 as a well-known, well studied tool for our research
\end_layout

\begin_layout Plain Layout

}
\end_layout

\end_inset


\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
rTMS - Studies
\end_layout

\end_inset


\end_layout

\begin_layout Frame

\end_layout

\begin_layout Frame
\begin_inset Float figure
placement t
wide false
sideways false
status collapsed

\begin_layout Plain Layout
\begin_inset Graphics
	filename /home/albert/Desktop/Lyxfiles/Pictures_for_Lyx/First_Thesis_report/rTMS in Stroop.png
	scale 18

\end_inset


\begin_inset Caption Standard

\begin_layout Plain Layout
rTMS studies abolishing physiological ACC function
\begin_inset Newline newline
\end_inset


\begin_inset Flex Uncover
status open

\begin_layout Plain Layout
\begin_inset Argument 1
status open

\begin_layout Plain Layout
2-
\end_layout

\end_inset


\begin_inset Flex Alert
status open

\begin_layout Plain Layout
Caution: Only one group
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Frame

\end_layout

\begin_layout Frame

\end_layout

\begin_layout Frame
Abolishment of Stroop effect in line with similar alteration of performance
 in Stroop task for cingulate-lesioned patients 
\begin_inset CommandInset citation
LatexCommand citep
key "Janer1991"

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Separator parbreak
\end_inset


\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
note[itemize]{
\end_layout

\begin_layout Plain Layout


\backslash
item Certain mistrust of the results as it is only done by them
\end_layout

\begin_layout Plain Layout

 but Stroop Interference can be effectively modulated if dACC is reached
\end_layout

\begin_layout Plain Layout

}
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Separator parbreak
\end_inset


\end_layout

\begin_layout Frame
\begin_inset Argument 2
status open

\begin_layout Plain Layout
+-
\end_layout

\end_inset


\begin_inset Argument 4
status open

\begin_layout Plain Layout
Stroop task design
\end_layout

\end_inset


\end_layout

\begin_layout Frame
Event-related Stroop task designed for fMRI experiment
\end_layout

\begin_layout Frame
\begin_inset Separator latexpar
\end_inset


\end_layout

\begin_deeper
\begin_layout Pause

\end_layout

\begin_layout Itemize
N = 600, 1.5
\begin_inset space ~
\end_inset

s, ISI: jittered; mean: 0.5
\begin_inset space ~
\end_inset

s
\end_layout

\begin_layout Itemize
congruent (N=300), incongruent trials
\end_layout

\begin_layout Itemize
Two-Alternative Forced Choice (2AFC) task
\end_layout

\begin_deeper
\begin_layout Itemize
four colours: green/red, blue/yellow 
\end_layout

\begin_layout Itemize
only two alternatives
\end_layout

\end_deeper
\begin_layout Itemize
can be modelled (Diffusion Model for Conflict Tasks)
\end_layout

\end_deeper
\begin_layout Standard
\begin_inset Separator parbreak
\end_inset


\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Drift Diffusion Model
\end_layout

\end_inset


\end_layout

\begin_layout Frame
well-established model for 2AFC tasks 
\begin_inset CommandInset citation
LatexCommand citep
key "Ratcliff2008"

\end_inset


\end_layout

\begin_layout Frame

\end_layout

\begin_layout Frame

\end_layout

\begin_layout Frame
\begin_inset Float figure
placement t
wide false
sideways false
status collapsed

\begin_layout Plain Layout
\begin_inset Graphics
	filename /home/albert/Desktop/Lyxfiles/Pictures_for_Lyx/Retreat/ratcliff.png
	scale 30

\end_inset


\begin_inset Caption Standard

\begin_layout Plain Layout
Drift Diffusion Model
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Separator parbreak
\end_inset


\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Diffusion Model for Conflict task (DMC)
\end_layout

\end_inset


\end_layout

\begin_layout Frame
based on Drift Diffusion Models 
\begin_inset CommandInset citation
LatexCommand citep
key "Ulrich2015"

\end_inset


\end_layout

\begin_layout Frame

\end_layout

\begin_layout Frame

\end_layout

\begin_layout Frame
\begin_inset Float figure
placement t
wide false
sideways false
status collapsed

\begin_layout Plain Layout
\begin_inset Graphics
	filename /home/albert/Desktop/Lyxfiles/Pictures_for_Lyx/Retreat/Ulrich_figure.png
	scale 40

\end_inset


\begin_inset Caption Standard

\begin_layout Plain Layout
stimulated DMC
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Frame

\end_layout

\begin_layout Section
Hypotheses
\end_layout

\begin_layout Standard
\begin_inset Separator parbreak
\end_inset


\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Hypotheses
\end_layout

\end_inset


\begin_inset Separator latexpar
\end_inset


\end_layout

\begin_deeper
\begin_layout Itemize
\begin_inset Flex Bold
status open

\begin_layout Plain Layout
\begin_inset Argument 1
status open

\begin_layout Plain Layout
2
\end_layout

\end_inset

tACS / tRNS suited for effectivly modulating deeper brain area activity
\end_layout

\end_inset


\end_layout

\begin_layout Itemize
\begin_inset Flex Bold
status open

\begin_layout Plain Layout
\begin_inset Argument 1
status open

\begin_layout Plain Layout
3
\end_layout

\end_inset

entrainment of tACS in Theta rhythm will lead to increased Stroop interference
\end_layout

\end_inset


\end_layout

\begin_layout Itemize
\begin_inset Flex Bold
status open

\begin_layout Plain Layout
\begin_inset Argument 1
status open

\begin_layout Plain Layout
4
\end_layout

\end_inset

tRNS induces changes in activity in dACC / change in Stroop interference
\end_layout

\end_inset


\end_layout

\end_deeper
\begin_layout Standard
\begin_inset Separator parbreak
\end_inset


\end_layout

\begin_layout Section
Study design
\end_layout

\begin_layout Standard
\begin_inset Separator parbreak
\end_inset


\end_layout

\begin_layout Subsection
Targeting the dorsal Anterior Cingulate Cortex
\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
dorsal Anterior Cingulate Cortex (dACC)
\end_layout

\end_inset


\end_layout

\begin_layout Frame
Brodman area 32
\end_layout

\begin_layout Frame
\begin_inset Float figure
wide false
sideways false
status collapsed

\begin_layout Plain Layout
\begin_inset Graphics
	filename /home/albert/Desktop/Lyxfiles/Pictures_for_Lyx/First_Thesis_report/Dorsal_Anterior_Cingulate.png
	scale 20

\end_inset


\begin_inset Caption Standard

\begin_layout Plain Layout
adapted from 
\begin_inset CommandInset citation
LatexCommand citet
key "Stevens"

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout

\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Separator parbreak
\end_inset


\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
note[itemize]{
\end_layout

\begin_layout Plain Layout


\backslash
item literature review if dACC is genauer definiert in den source localization
 papern
\end_layout

\begin_layout Plain Layout

}
\end_layout

\end_inset


\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Montages
\end_layout

\end_inset


\begin_inset Separator latexpar
\end_inset


\end_layout

\begin_deeper
\begin_layout Itemize
Stimulating deeper brain areas necessarily will induce electric fields in
 cortical areas.
 
\end_layout

\begin_deeper
\begin_layout Itemize
FC3 / FC4 as first targets (10-20 system)
\end_layout

\end_deeper
\begin_layout Itemize
Multiple montages will be tested for their effectiveness in inducing changes
 during the Stroop task.
 
\end_layout

\begin_layout Itemize
By sequentially combining the montages in a temporal manner, cortical brain
 areas will be less stimulated while targeting the deeper dorsal ACC
\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status collapsed

\begin_layout Plain Layout
\begin_inset Graphics
	filename /home/albert/Desktop/Lyxfiles/Pictures_for_Lyx/First_Thesis_report/Best_targeting.png
	scale 15

\end_inset


\end_layout

\begin_layout Plain Layout

\end_layout

\end_inset


\end_layout

\end_deeper
\begin_layout Frame

\end_layout

\begin_layout Standard
\begin_inset Separator parbreak
\end_inset


\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Montages
\end_layout

\end_inset


\begin_inset Separator latexpar
\end_inset


\end_layout

\begin_deeper
\begin_layout Itemize
Stimulating deeper brain areas necessarily will induce electric fields in
 cortical areas.
 
\end_layout

\begin_deeper
\begin_layout Itemize
FC3 / FC4 as first targets (10-20 system)
\end_layout

\end_deeper
\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status open

\begin_layout Plain Layout
\begin_inset Graphics
	filename /home/albert/Desktop/Lyxfiles/Pictures_for_Lyx/Retreat/FC5FC6
	scale 15

\end_inset


\end_layout

\end_inset


\end_layout

\end_deeper
\begin_layout Frame

\end_layout

\begin_layout Standard
\begin_inset Separator parbreak
\end_inset


\end_layout

\begin_layout Subsection
Study design
\end_layout

\begin_layout Standard
\begin_inset Separator parbreak
\end_inset


\end_layout

\begin_layout Frame
\begin_inset Argument 2
status open

\begin_layout Plain Layout
+-
\end_layout

\end_inset


\begin_inset Argument 4
status open

\begin_layout Plain Layout
Study design
\end_layout

\end_inset


\begin_inset Separator latexpar
\end_inset


\end_layout

\begin_deeper
\begin_layout Itemize
healthy, gender-age-balanced, non-colourblind, german participants
\end_layout

\begin_layout Itemize
double-blinded
\end_layout

\begin_layout Itemize
trial groups (n
\begin_inset space ~
\end_inset

=
\begin_inset space ~
\end_inset

8), expanding to n
\begin_inset space ~
\end_inset

=
\begin_inset space ~
\end_inset

20
\end_layout

\begin_layout Itemize
crossover study, sham-controlled
\end_layout

\begin_layout Itemize
tACS (6
\begin_inset space ~
\end_inset

Hz) and tRNS in seperate groups
\end_layout

\begin_layout Itemize
multiple electrode montages
\end_layout

\begin_layout Itemize
RT and Accuracy as behavioural readouts
\end_layout

\begin_layout Itemize
fMRI and EEG as physiological readout
\end_layout

\begin_layout Itemize
Ethics proposal accepted (19/1/17)
\end_layout

\end_deeper
\begin_layout Standard
\begin_inset Separator parbreak
\end_inset


\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Stimulation paradigm
\end_layout

\end_inset


\begin_inset Separator latexpar
\end_inset


\end_layout

\begin_deeper
\begin_layout Itemize
tACS: frequency individualizing possible, but laborious
\begin_inset Separator latexpar
\end_inset


\end_layout

\begin_deeper
\begin_layout Itemize
tACS: 6
\begin_inset space ~
\end_inset

Hz as common frequency possible
\end_layout

\end_deeper
\begin_layout Itemize
tRNS: over complete frequency range
\end_layout

\begin_layout Itemize
Theta-range tRNS: randomly distributed frequencies in 4
\begin_inset space ~
\end_inset

to
\begin_inset space ~
\end_inset

7
\begin_inset space ~
\end_inset

Hz range
\end_layout

\end_deeper
\begin_layout Standard
\begin_inset Separator parbreak
\end_inset


\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
note[itemize]{
\end_layout

\begin_layout Plain Layout


\backslash
item possibility to individualize tACS theta frequency, but it could also
 be that the frequency which gets activated changes during learning of the
 task => so maybe not beeter to indivizualize it
\end_layout

\begin_layout Plain Layout


\backslash
item we can also use a random noise stimulation in the theta range (pseudorandom
 noise stimulation)
\end_layout

\begin_layout Plain Layout

}
\end_layout

\end_inset


\end_layout

\begin_layout Section
Results & Outline
\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
mode<handout:0 |presentation:0>{ % turn presenation:1 to turn it on
\end_layout

\begin_layout Plain Layout


\backslash
subsection{Expected results}
\end_layout

\begin_layout Plain Layout


\backslash
begin{frame}{Aim}
\end_layout

\begin_layout Plain Layout


\backslash
itemize{
\end_layout

\begin_layout Plain Layout


\backslash
item Entrainment of dACC will decrease the Stroop effect
\end_layout

\begin_layout Plain Layout

}
\end_layout

\begin_layout Plain Layout


\backslash
end{frame}
\end_layout

\begin_layout Plain Layout

\end_layout

\begin_layout Plain Layout

}
\end_layout

\begin_layout Plain Layout


\backslash
subsection{Results}
\end_layout

\begin_layout Plain Layout

\end_layout

\end_inset


\begin_inset Separator parbreak
\end_inset


\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Results
\end_layout

\end_inset


\end_layout

\begin_layout Frame

\end_layout

\begin_layout Frame

\end_layout

\begin_layout Frame
Cumulative distribution function
\end_layout

\begin_layout Frame
\begin_inset Float figure
placement t
wide false
sideways false
status open

\begin_layout Plain Layout
\begin_inset Graphics
	filename /home/albert/Desktop/Lyxfiles/Pictures_for_Lyx/Retreat/CDP.png
	scale 30

\end_inset


\begin_inset Caption Standard

\begin_layout Plain Layout
N = 8
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Separator parbreak
\end_inset


\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Results
\end_layout

\end_inset


\end_layout

\begin_layout Frame

\end_layout

\begin_layout Frame

\end_layout

\begin_layout Frame
Reaction time distribution
\end_layout

\begin_layout Frame
\begin_inset Float figure
placement t
wide false
sideways false
status collapsed

\begin_layout Plain Layout
\begin_inset Graphics
	filename /home/albert/Desktop/Lyxfiles/Pictures_for_Lyx/Retreat/AvsB.png
	scale 30

\end_inset


\begin_inset Caption Standard

\begin_layout Plain Layout
N = 8
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Separator parbreak
\end_inset


\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Results
\end_layout

\end_inset


\end_layout

\begin_layout Frame

\end_layout

\begin_layout Frame

\end_layout

\begin_layout Frame
Delta plot
\end_layout

\begin_layout Frame
\begin_inset Float figure
placement t
wide false
sideways false
status collapsed

\begin_layout Plain Layout
\begin_inset Graphics
	filename /home/albert/Desktop/Lyxfiles/Pictures_for_Lyx/Retreat/Deltaplot.png
	scale 30

\end_inset


\begin_inset Caption Standard

\begin_layout Plain Layout
N = 8
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Separator parbreak
\end_inset


\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
subsection{Workflow}
\end_layout

\end_inset


\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Further workflow
\end_layout

\end_inset


\end_layout

\begin_layout Frame

\end_layout

\begin_deeper
\begin_layout Overprint
\begin_inset Argument item:1
status open

\begin_layout Plain Layout
1|handout:1
\end_layout

\end_inset


\begin_inset Separator latexpar
\end_inset


\end_layout

\begin_deeper
\begin_layout Block
\begin_inset Argument 2
status open

\begin_layout Plain Layout
Preliminary work (n=8)
\end_layout

\end_inset


\end_layout

\begin_layout Itemize
Test several montages that affect behavioural readout with both tACS / tRNS
\end_layout

\begin_layout Itemize
compare behavioural results to sham
\end_layout

\begin_layout Itemize
combination of most effective montages in sequential-temporal manner
\end_layout

\end_deeper
\begin_layout Overprint
\begin_inset Argument item:1
status open

\begin_layout Plain Layout
2|handout:2
\end_layout

\end_inset


\begin_inset Separator latexpar
\end_inset


\end_layout

\begin_deeper
\begin_layout Block
\begin_inset Argument 2
status open

\begin_layout Plain Layout
fMRI - Study (n = 20)
\end_layout

\end_inset


\end_layout

\begin_layout Itemize
utilize most effective stimulation approach
\end_layout

\begin_layout Itemize
Search biomarkers of modulation by stimulation in MRI
\begin_inset Separator latexpar
\end_inset


\end_layout

\begin_deeper
\begin_layout Itemize
BOLD
\end_layout

\begin_layout Itemize
functional network connectivity
\end_layout

\begin_layout Itemize
Arterial spin labeling
\end_layout

\end_deeper
\begin_layout Itemize
Investigation of variability factors (bone and CSF thickness)
\end_layout

\begin_layout Itemize
MRI
\begin_inset space ~
\end_inset

-
\begin_inset space ~
\end_inset

Führerschein
\end_layout

\end_deeper
\begin_layout Overprint
\begin_inset Argument item:1
status open

\begin_layout Plain Layout
3|handout:3
\end_layout

\end_inset


\begin_inset Separator latexpar
\end_inset


\end_layout

\begin_deeper
\begin_layout Block
\begin_inset Argument 2
status open

\begin_layout Plain Layout
EEG - Study (n = 20)
\end_layout

\end_inset


\end_layout

\begin_layout Itemize
most effective montage tested 
\end_layout

\begin_layout Itemize
temporal network analysis
\end_layout

\begin_layout Itemize
electrial stimulation can be linked to stimulus onset
\end_layout

\end_deeper
\end_deeper
\begin_layout Frame

\end_layout

\begin_layout Frame

\end_layout

\begin_layout Standard
\begin_inset Separator parbreak
\end_inset


\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
note[itemize]{
\end_layout

\begin_layout Plain Layout


\backslash
item Describe better what has to be done before doing the fMRI
\end_layout

\begin_layout Plain Layout


\backslash
item not measuring BOLD / functional network connection but BIOMARKERS are
 looked for, we can also use ASL
\end_layout

\begin_layout Plain Layout


\backslash
item Arterial spin labeling: measures Blood flow like PET but without radioactiv
ity
\end_layout

\begin_layout Plain Layout


\backslash
item also mention that in the fMRI we are going to look at variability and
 that we are going to measure CSF and bone thickness in humans
\end_layout

\begin_layout Plain Layout


\backslash
 we are going to try and explain variablity
\end_layout

\begin_layout Plain Layout

}
\end_layout

\end_inset


\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Thank you for your attention
\end_layout

\end_inset


\end_layout

\begin_layout Frame
\begin_inset Float figure
placement t
wide false
sideways false
status collapsed

\begin_layout Plain Layout
\begin_inset Graphics
	filename /home/albert/Desktop/Lyxfiles/Pictures_for_Lyx/Danksagung/Research_group.JPG
	lyxscale 10
	scale 15

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Separator parbreak
\end_inset


\end_layout

\begin_layout AgainFrame
mytitle
\end_layout

\begin_layout Standard
\begin_inset Separator parbreak
\end_inset


\end_layout

\begin_layout Section
\start_of_appendix
Appendix
\end_layout

\begin_layout Standard
\begin_inset Note Note
status open

\begin_layout Plain Layout
We use a FrameTitle layout (and not the Frame Title inset) below in order
 to exclude the redundant title from the article.
 Also note the 
\begin_inset Flex Alert
status collapsed

\begin_layout Plain Layout

\backslash
newblock
\end_layout

\end_inset

 in the bibliography.
 It is needed for proper entry rendering (note the different coloring of
 the author in the PDF output).
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
begin{frame}[allowframebreaks]
\end_layout

\begin_layout Plain Layout


\backslash
frametitle{References}
\end_layout

\begin_layout Plain Layout


\backslash
fontsize{6pt}{7.2}
\backslash
selectfont
\end_layout

\begin_layout Plain Layout

\end_layout

\end_inset


\begin_inset CommandInset bibtex
LatexCommand bibtex
bibfiles "/home/albert/Desktop/Lyxfiles/Doctoral_references"
options "humannat"

\end_inset


\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
end{frame}
\end_layout

\end_inset


\end_layout

\end_body
\end_document
