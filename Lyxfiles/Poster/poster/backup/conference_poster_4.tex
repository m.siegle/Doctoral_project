%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% baposter Landscape Poster
% LaTeX Template
% Version 1.0 (11/06/13)
%
% baposter Class Created by:
% Brian Amberg (baposter@brian-amberg.de)
%
% This template has been downloaded from:
% http://www.LaTeXTemplates.com
%
% License:
% CC BY-NC-SA 3.0 (http://creativecommons.org/licenses/by-nc-sa/3.0/)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%----------------------------------------------------------------------------------------
%	PACKAGES AND OTHER DOCUMENT CONFIGURATIONS
%----------------------------------------------------------------------------------------

\documentclass[landscape,a0paper,fontscale=0.285]{baposter} % Adjust the font scale/size here


\usepackage{graphicx} % Required for including images
\graphicspath{{figures/}} % Directory in which figures are stored

\usepackage{amsmath} % For typesetting math
\usepackage{amssymb} % Adds new symbols to be used in math mode
\usepackage{xcolor}
\usepackage{booktabs} % Top and bottom rules for tables
\usepackage{enumitem} % Used to reduce itemize/enumerate spacing
\usepackage{palatino} % Use the Palatino font
\usepackage[font=small,labelfont=bf]{caption} % Required for specifying captions to tables and figures

\usepackage{multicol} % Required for multiple columns
\setlength{\columnsep}{1.5em} % Slightly increase the space between columns
\setlength{\columnseprule}{0mm} % No horizontal rule between columns

\usepackage{tikz} % Required for flow chart
\usetikzlibrary{shapes,arrows} % Tikz libraries required for the flow chart in the template

\newcommand{\compresslist}{ % Define a command to reduce spacing within itemize/enumerate environments, this is used right after \begin{itemize} or \begin{enumerate}
\setlength{\itemsep}{1pt}
\setlength{\parskip}{0pt}
\setlength{\parsep}{0pt}
}

\definecolor{lightblue}{rgb}{0.145,0.6666,1} % Defines the color used for content box headers

\begin{document}

\begin{poster}
{
headerborder=closed, % Adds a border around the header of content boxes
colspacing=1em, % Column spacing
bgColorOne=white, % Background color for the gradient on the left side of the poster
bgColorTwo=white, % Background color for the gradient on the right side of the poster
borderColor=lightblue, % Border color
headerColorOne=black, % Background color for the header in the content boxes (left side)
headerColorTwo=lightblue, % Background color for the header in the content boxes (right side)
headerFontColor=white, % Text color for the header text in the content boxes
boxColorOne=white, % Background color of the content boxes
textborder=roundedleft, % Format of the border around content boxes, can be: none, bars, coils, triangles, rectangle, rounded, roundedsmall, roundedright or faded
eyecatcher=false, % Set to false for ignoring the left logo in the title and move the title left
headerheight=0.15\textheight, % Height of the header
headershape=roundedright, % Specify the rounded corner in the content box headers, can be: rectangle, small-rounded, roundedright, roundedleft or rounded
headerfont=\Large\bf\textsc, % Large, bold and sans serif font in the headers of content boxes
%textfont={\setlength{\parindent}{1.5em}}, % Uncomment for paragraph indentation
linewidth=2pt % Width of the border lines around content boxes
}
%----------------------------------------------------------------------------------------
%	TITLE SECTION 
%----------------------------------------------------------------------------------------
{\includegraphics[height=4em]{white.jpg}} % First university/lab logo on the left
{\bf{Transcranial electrical stimulation during Stroop task}\vspace{0.3em}} % Poster title
{\small{ Albert Lehr$^{1}$, Micha Siegle$^{1}$, Walter Paulus$^{1}$ and Andrea Antal$^{1}$  \hspace*{\fill}\linebreak $^{1}$Clinic for Clinical Neurophysiology, University of  G\"ottingen}} % Author names and institution
{\includegraphics[height=4em]{GGNB3.jpg}} % Second university/lab logo on the right

%----------------------------------------------------------------------------------------
%	OBJECTIVES
%----------------------------------------------------------------------------------------

\headerbox{Introduction \& Objectives}{name=objectives,column=0, span= 2,row=0}{

The cognitive control network (CCN) is involved in the Stroop task, in which subjects have to resolve a conflict between task-relevant and task-irrelevant
stimulus dimensions. The oscillatory phase coupling of two network hubs, the \textbf{dorsolateral prefrontal cortex (DLPFC)} and the \textbf{anterior cingulate cortex (ACC)},
increases in incongruent trials but the casual relationship remains elusive \cite{Hanslmayr2008}. \\[0.5em]
\textbf{Transcranial electrical brain stimulation (tEBS)} induces changes in brain function non-invasively. Alternating current stimulation (tACS) changes phase and power of neural 
oscillations; noise current stimulation (tRNS) unspecifically amplifies resonance of neural networks to external input. However, targeting deeper brain areas remains a challenge.
We \textbf{targeted the ACC}, which phase couples in Theta range with the DLPFC during incongruent trials in the Stroop Task.
\begin{multicols}{3}




\begin{itemize}\compresslist
\item Stroop effect: response time difference between congruent and incongruent trials
\item Gratton effect: preceding incongruent trial reduces Stroop effect in trial
\item increased ACC + DLPFC theta phase coupling during conflict trial
\end{itemize}
\vfill
\textbf{Hypotheses}
\begin{itemize}\compresslist
\item tEBS increases ACC + DLPFC Theta phase coupling 
\item externally induced Gratton effect reduces Stroop effect
\item externally induced Gratton effect in trials preceded by congruent
\end{itemize}

\begin{center}
\includegraphics[width=0.65\linewidth]{Selection_006.png}
\captionof{figure}{Gratton Effect}
\end{center}
\end{multicols}

\vspace{0.3em} % When there are two boxes, some whitespace may need to be added if the one on the right has more content
}


%----------------------------------------------------------------------------------------
%	RESULTS 1
%----------------------------------------------------------------------------------------

\headerbox{Results}{name=results,column=2,span=2,row=0}{\setlength{\emergencystretch}{10pt}



\begin{center}
\begin{tabular}{l l l l l}
\toprule
\textbf{Conditions} & \textbf{4 Hz} & \textbf{6 Hz} & \textbf{Noise}& \textbf{Sham}\\
\midrule
Accuracy in \%            &                      &                      &                      & \\
\hspace{1em}Congruent     &96.4 $\pm$ 3.3        &96.7 $\pm$ 2.5        &97.3 $\pm$ 2.3        &96.8$\pm$ 2.4\\
\hspace{1em}Incongruent   &95.8 $\pm$ 3.4        &96.1 $\pm$ 2.6        &96.1$\pm$ 2.6         & 95.8 $\pm$ 3.5\\
Reaction Times in s &  & & & \\
\hspace{1em}Congruent     &0.574 $\pm$ 0.135     &0.579 $\pm$ 0.141     &0.560 $\pm$ 0.131     &0.577 $\pm$ 0.137 \\
\hspace{1em}Incongruent   &0.598 $\pm$ 0.145     &0.601$\pm$ 0.148      &0.580 $\pm$ 0.140     &0.605 $\pm$ 0.153\\
Stroop Effect             &\textbf{0.027 s}      &\textbf{0.025 s}      &\textbf{0.028 s}      & \textbf{0.025 s}\\
\bottomrule
\end{tabular}
\captionof{table}{Behavioral Results of Main Stroop Phase}
\end{center}
%------------------------------------------------

\begin{multicols}{2}
\vspace{1em}

\begin{center}
\includegraphics[width=0.8\linewidth]{Behavior_RT.png}
\captionof{figure}{Stroop Effect}
\end{center}




\begin{center}
\includegraphics[width=0.8\linewidth]{Bahavior_Conflict.png}
\captionof{figure}{Gratton Effect}
\end{center}

\end{multicols}
}

%----------------------------------------------------------------------------------------
%	REFERENCES
%----------------------------------------------------------------------------------------

\headerbox{References}{name=references,column=0,span=3,above=bottom}{

\renewcommand{\section}[2]{\vskip 0.05em} % Get rid of the default "References" section title
%\nocite{*} % Insert publications even if they are not cited in the poster
\small{ % Reduce the font size in this block
\bibliographystyle{unsrt}
\bibliography{sample} % Use sample.bib as the bibliography file
}}


%----------------------------------------------------------------------------------------
%	CONTACT INFORMATION
%----------------------------------------------------------------------------------------

\headerbox{Contact Information}{name=contact,column=3,aligned=references,above=bottom}{ % This block is as tall as the references block

\begin{description}\compresslist
\item[Web] \hfill neurologie.uni-goettingen.de
\item[Email] \hfill albertlehr@gmx.de
\end{description}
}

%----------------------------------------------------------------------------------------
%	CONCLUSION
%----------------------------------------------------------------------------------------

\headerbox{Conclusion}{name=conclusion,column=2,span=2,row=0,below=results,above=references}{

\begin{multicols}{2}



%------------------------------------------------

\begin{itemize}\compresslist
\item The behavioral data does not support the hypothesis that applying tEBS to the ACC during Stroop Task induces changes in Stroop or Gratton effect. Neither the accuracy nor the response times  are significantly changed. 
\item The tEBS electrode setup and frequency were based on previous research that even showed effects for targeting ACC in Stroop Task with tEBS. Nonetheless, in absence of EEG data, the effect of tEBS on ACC activity cannot be assessed. 
\item Further analysis of the EEG data will yield greater insights into results, as changes in electrophysiological data is often measurable even in absence of behavioral effects.
\end{itemize}

\end{multicols}
}

%----------------------------------------------------------------------------------------
%	MATERIALS AND METHODS
%----------------------------------------------------------------------------------------

\headerbox{Materials \& Methods}{name=method,column=0,span=2,below=objectives,bottomaligned=conclusion}{ % This block's bottom aligns with the bottom of the conclusion block

\begin{multicols}{2}

\tikzstyle{decision} = [diamond, draw, fill=blue!20, text width=4.5em, text badly centered, node distance=2cm, inner sep=0pt]
\tikzstyle{block} = [rectangle, draw, fill=red!20, text width=5em, text centered, rounded corners, minimum height=4em]
\tikzstyle{block_trial} = [rectangle, draw, fill=blue!20, text width=5em, text centered, rounded corners, minimum height=4em]
\tikzstyle{line} = [draw, -latex']
\tikzstyle{cloud} = [draw, ellipse, fill=red!20, node distance=3.3cm, minimum height=2em]

\hspace{4em}\begin{tikzpicture}[node distance = 3cm, auto]
\node [block] (init) {tEBS\\Stroop\\20 min};
\node [block, left of=init] (Start) {EEG\\Stroop\\10 min};
\node [block, right of=init] (Start2) {EEG\\Stroop\\10 min};
\node [block_trial, below of=init] (init2) {\textcolor{red}{Blue}\\1.5 s\\Response};
\node [block_trial, right of=init2] (End) {Fixation\\0.5 s};
\node[block_trial, left of= init2] (Fix) {Fixation\\ 0.5 s};
\draw[->, blue!50, very thick] (init) to[bend left=10]
node[above] {.} (Fix);
\draw[->, blue!50, very thick] (End) to[bend left=10]
node[above] {.} (init);
\path [line] (init2) -- (End);
\path [line] (Start) -- (init);
\path [line] (init) -- (Start2);
\path [line] (Fix) --(init2);
\end{tikzpicture}


\begin{center}

\begin{tabular}{l l l}
\toprule
\textbf{tEBS Conditions}  \\
\midrule
tACS - 4 Hz   \\
tACS - 6 Hz   \\
tRNS - 0 - 1000 Hz  \\
Sham (Control) \\
\bottomrule
\end{tabular}
\end{center}

\end{multicols}
\textbf{16 Partcipants} were measured in a crossover design, participating in all tEBS conditions. Accuracy and reaction times of behavioral responses were measured and analyzed. The EEG analysis is not finished.\\[1em]
}


%----------------------------------------------------------------------------------------

\end{poster}

\end{document}