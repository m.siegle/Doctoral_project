#LyX 2.2 created this file. For more info see http://www.lyx.org/
\lyxformat 508
\begin_document
\begin_header
\save_transient_properties true
\origin unavailable
\textclass article
\begin_preamble
\usepackage{tocbibind}

\usepackage {url}
%\renewcommand\usepackage [round,authoryear]{natbib}

\usepackage{scrpage2}
\usepackage{graphicx}
\usepackage{amssymb}
\usepackage{booktabs}
\usepackage{nomencl}
\usepackage{booktabs}
\usepackage{multirow}
\usepackage{textfit}
\usepackage{hyphenat}
\usepackage{float}
\usepackage{rotfloat}
\usepackage{floatflt}
\usepackage{rotating}
\usepackage{color}
%\usepackage[utf8]{inputenc}
%\usepackage[normalmargins,normallists]{savetrees} %package that reduces "white space"
\usepackage[footnotesize]{caption}
\usepackage{url}
\urlstyle{tt}
\usepackage[version=3]{mhchem}

\linespread{1.5}
\usepackage{acronym}
\usepackage{booktabs}
\usepackage[bookmarksopenlevel=section,colorlinks, linkcolor = black, citecolor = blue, filecolor = black, urlcolor = black]{hyperref}

\pagestyle{scrheadings} 
%\renewcommand\bibliographystyle{humannat}
\usepackage{tikz}
\end_preamble
\use_default_options true
\maintain_unincluded_children false
\language english
\language_package default
\inputencoding utf8
\fontencoding global
\font_roman "default" "default"
\font_sans "default" "default"
\font_typewriter "default" "default"
\font_math "auto" "auto"
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100 100
\font_tt_scale 100 100
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\float_placement H
\paperfontsize 12
\spacing onehalf
\use_hyperref false
\papersize default
\use_geometry true
\use_package amsmath 1
\use_package amssymb 1
\use_package cancel 1
\use_package esint 1
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 1
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine natbib
\cite_engine_type authoryear
\biblio_style abbrvnat
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date true
\justification true
\use_refstyle 1
\index Index
\shortcut idx
\color #008000
\end_index
\leftmargin 2.5cm
\topmargin 2cm
\rightmargin 2.5cm
\bottommargin 2cm
\secnumdepth 3
\tocdepth 4
\paragraph_separation skip
\defskip smallskip
\quotes_language english
\papercolumns 1
\papersides 1
\paperpagestyle headings
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
begin{titlepage}
\backslash
pagenumbering{Roman}
\end_layout

\begin_layout Plain Layout


\backslash
setcounter{page}{1}
\backslash
addcontentsline{toc}{section}{Frontpage}
\end_layout

\end_inset


\begin_inset space \hspace{}
\length 9cm
\end_inset


\begin_inset Newline newline
\end_inset

 
\begin_inset Graphics
	filename UMG.jpg
	lyxscale 10
	scale 25

\end_inset


\begin_inset space \hspace{}
\length 37page%
\end_inset


\begin_inset Graphics
	filename GGNB3.jpg
	lyxscale 10
	scale 3

\end_inset


\end_layout

\begin_layout Standard
\begin_inset CommandInset line
LatexCommand rule
offset "0.5ex"
width "100text%"
height "1pt"

\end_inset


\end_layout

\begin_layout Standard
\begin_inset VSpace 1.5cm
\end_inset


\end_layout

\begin_layout Standard
\align center
\begin_inset ERT
status open

\begin_layout Plain Layout

{
\end_layout

\end_inset


\series bold
\size larger

\begin_inset ERT
status open

\begin_layout Plain Layout

{
\end_layout

\end_inset

Second progress report
\begin_inset ERT
status open

\begin_layout Plain Layout

}
\end_layout

\end_inset

 
\series default
\size default

\begin_inset ERT
status open

\begin_layout Plain Layout

}
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\align center
\begin_inset ERT
status open

\begin_layout Plain Layout

{
\end_layout

\end_inset

- 18 months after start of thesis project -
\begin_inset ERT
status open

\begin_layout Plain Layout

}
\end_layout

\end_inset

 
\begin_inset VSpace 1.5cm
\end_inset


\end_layout

\begin_layout Standard
\paragraph_spacing onehalf
Name: Albert Lehr
\end_layout

\begin_layout Standard
\paragraph_spacing onehalf
GGNB program: Neurosciences
\end_layout

\begin_layout Standard
\paragraph_spacing onehalf
Date of progress report: 16.03.
 2018
\end_layout

\begin_layout Standard
\paragraph_spacing onehalf
Date of thesis advisory committee meeting: 19.03.
 2018
\end_layout

\begin_layout Standard
Supervisor: Prof.
 Andrea Antal
\end_layout

\begin_layout Standard
\paragraph_spacing onehalf
2nd and 3rd member of thesis advisory committee: 
\end_layout

\begin_layout Standard
\paragraph_spacing onehalf
\begin_inset space \qquad{}
\end_inset

Arezoo Pooresmaeili, Ph.D.
\end_layout

\begin_layout Standard
\paragraph_spacing onehalf
\begin_inset space \qquad{}
\end_inset

Prof.
 Susann Boretius 
\end_layout

\begin_layout Standard
\paragraph_spacing onehalf
Title of project: 
\end_layout

\begin_layout Standard
\begin_inset Quotes eld
\end_inset

Modulation of neuronal excitability in deep brain areas by electric stimulation
\begin_inset Quotes erd
\end_inset


\end_layout

\begin_layout Standard
\paragraph_spacing onehalf
Starting date of thesis: 01.10.
 2016 
\end_layout

\begin_layout Standard
\paragraph_spacing onehalf
Deadline for submission of thesis: 30.09.
 2019
\begin_inset Separator latexpar
\end_inset


\end_layout

\begin_layout Standard
Date of first thesis advisory committee meeting: 24.3.
 2017 
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash

\backslash

\end_layout

\end_inset


\end_layout

\begin_layout Standard
Credits gathered so far:
\end_layout

\begin_layout Standard
\begin_inset space \qquad{}
\end_inset

- lectures, colloquia, seminars (min.
 5 C):
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
hfill
\end_layout

\end_inset

2 C 
\end_layout

\begin_layout Standard
\begin_inset space \qquad{}
\end_inset

- methods courses (min.
 2 C):
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
hfill
\end_layout

\end_inset

4.5 C
\end_layout

\begin_layout Standard
\begin_inset space \qquad{}
\end_inset

- teaching (min.
 4 C):
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
hfill
\end_layout

\end_inset

2.5 C
\end_layout

\begin_layout Standard
\begin_inset space \qquad{}
\end_inset

- scientific meetings (min.
 2 C):
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
hfill
\end_layout

\end_inset

4.5 C
\end_layout

\begin_layout Standard
\begin_inset space \qquad{}
\end_inset

- key qualifications (min.
 1 C):
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
hfill
\end_layout

\end_inset

7 C
\end_layout

\begin_layout Standard
\begin_inset CommandInset line
LatexCommand rule
offset "0.5ex"
width "100text%"
height "1pt"

\end_inset


\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
end{titlepage}
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Newpage pagebreak
\end_inset


\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
pagenumbering{Roman}
\end_layout

\begin_layout Plain Layout


\backslash
setcounter{page}{2}
\end_layout

\end_inset


\end_layout

\begin_layout Section*
Modulation of neural excitability in deep brain areas by electric stimulation
\begin_inset CommandInset label
LatexCommand label
name "sec:Modulation-of-neural"

\end_inset


\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
addcontentsline{toc}{section}{Report}
\end_layout

\end_inset


\end_layout

\begin_layout Section*
Introduction
\end_layout

\begin_layout Standard
Transcranial electrical brain stimulation (tEBS) is a promising technique
 to induce changes in patients and healthy subjects while minimizing risks.
 It has been used increasingly to treat psychological and neurological diseases
 as depression and stroke, respectivly 
\begin_inset CommandInset citation
LatexCommand citep
key "Lefaucheur2017"

\end_inset

.
 However, mostly cortical areas have been the target of these stimulation
 protocols; the induction of changes in neural activity in deeper brain
 areas has still not been adequately adressed.
\end_layout

\begin_layout Standard
The goal is to explore the possibility of applying low-intensity tEBS to
 change the activity of deeper brain structures.
 Transcranial alternating current stimulation (tACS) and transcranial random
 noise stimulation (tRNS) will be the two applied stimulation techniques.
 As it is easily applicable, non-invasive, affordable and painless, it proves
 an intriguing technique.
 Still, changing brain activity in deeper brain areas with relatively small
 induced electrical field sizes is a challenging task.
 We chose the performance in the classical Stroop Task as the cognitive
 readout for changes in the cognitive control network 
\begin_inset CommandInset citation
LatexCommand citep
key "stroop1935studies"

\end_inset

.
 We aim to influence its function by targeting one of its network hubs,
 the dorsal Anterior Cingulate Cortex (dACC), using tEBS.
\end_layout

\begin_layout Section*
Current state of the project
\end_layout

\begin_layout Standard
During the last year I helped in the data collection for a study of my colleague
 Gabriel de Lara.
 We studied the influence of tACS on the longterm explicit memory of healthy
 participants using a special theta-gamma coupled waveform 
\begin_inset CommandInset citation
LatexCommand citep
key "Lara2018a"

\end_inset

.
\end_layout

\begin_layout Standard
After last year's meeting I used the blocked Stroop Task design in two pilot
 studies and stimulated eight participants each (1) with tACS and (2) tRNS.
 Aiming to gather experience for employing a rotating stimulation order,
 a simple montage with electrodes at FC3 and FC4 (10-20 system) was used.
 In both pilot studies neither the mean reaction times nor the accuracies
 led us to conclude that conflict processing was significantly changed.
\end_layout

\begin_layout Standard
In order to increase the power of the Stroop task of a cognitive readout,
 the newly developed diffusion model for conflict task (DMC) was adopted
 
\begin_inset CommandInset citation
LatexCommand citep
key "Ulrich2015"

\end_inset

.
 The Stroop Task itself was altered to incooperate 300 incongruent and congruent
 trials each as many trials are needed to apply the DMC.
 The DMC is a variation of the drift diffusion model commonly used in two-altern
ative forced choice tasks 
\begin_inset CommandInset citation
LatexCommand citep
key "ratcliff1978theory"

\end_inset

.
 It describes the decision process as evidence gathering by a controlled
 process (colour recognition) and an automatic process (reading) throughout
 the decision phase.
 In the controlled process evidence is gathered consistently.
 On the other side, the evidence gathering by the automatic reading is changing
 over time and it is contradictory to the controlled process in incongruent
 trials.
 
\end_layout

\begin_layout Standard
The DMC accounts for complete distribution of reaction times of correct
 and incorrect trials.
 It calculates parameters describing the controlled and the automatic process.
 Whereas the parameters of the controlled process describe two-alternative
 forced choices in genral, the parameters of the automatic process give
 insights into the conflict processing.
 The model has been validated to yield high correlations for real and modelled
 parameters with the current number of trials per participant similar to
 
\begin_inset CommandInset citation
LatexCommand citep
key "White2017"

\end_inset

.
\end_layout

\begin_layout Standard
One pilot study (n
\begin_inset space ~
\end_inset

=
\begin_inset space ~
\end_inset

8) using the new task was conducted with the same stimulation montage as
 in the two earlier pilot studies.
 Still, no evidence was found in the crude readouts mean reaction time and
 accuracy, but also the underlying parameters showed no evidence between
 real and sham stimulation.
 
\end_layout

\begin_layout Standard
As a control experiment, ten participants were stimulated with tACS at 6
\begin_inset space ~
\end_inset

Hz using a montage over the left dorsolateral prefrontal Cortex (lDLPFC),
 which is functionally connected to the dACC during Stroop task 
\begin_inset CommandInset citation
LatexCommand citep
key "Hanslmayr2008"

\end_inset

.
 Similarly, no evidence changes in conflict processing was found.
 The variability in non-decision time was significantly changed, but it
 is a parameter unrelated to the conflict and decision processing.
\end_layout

\begin_layout Subsection*
Outlook
\end_layout

\begin_layout Standard
The higher power of the new Stroop task and the DMC to detect changes in
 the conflict processing by applying tEBS have not let to significant results.
 Recent studies in the field have similarly not resulted in behavioral differenc
es, but found effects of tEBS in physiological readouts 
\begin_inset CommandInset citation
LatexCommand citep
key "nikolin2018effects"

\end_inset

.
 It is therefore conceivable that possible electrophysiological changes
 were induced without changing the behavioral readouts.
\end_layout

\begin_layout Standard
Moving ahead I am going to work closely with a medical doctoral student
 to execute a study with 20 participants.
 EEG will be recorded before and after the transcranial stimulation.
 These EEG recordings could be done either in resting or during Stroop Task
 
\begin_inset CommandInset citation
LatexCommand citep
key "Ergen2014"

\end_inset

.
\end_layout

\begin_layout Standard
As the pilot studies do not clearly indicate which montage, stimulation
 method or intensity might lead to behavioral effects an amendment to the
 original ethics proprosal has been written.
 It will be possible to measure 20 participants during four sessions each.
 A more medial montage combined with sham stimulation and three different
 stimulation strategies can be used.
\end_layout

\begin_layout Standard
Additionally, the idea of using two or more stimulation montages transiently
 to induce a neurophysiological change will be tested in Stroop Task during
 fMRI measurements.
 
\end_layout

\begin_layout Section*
Side project
\end_layout

\begin_layout Standard
In cooperation with Dr.
 Steinmann from the Institute for Cognitive Neurology a study combining
 tACS with both fMRI and EEG has been planned.
 Ethics approval was already given.
 fMRI recordings will be made throughout the experiment with simultaneous
 EEG recordings before and after the tACS stimulation.
\end_layout

\begin_layout Standard
We plan to investigate the effect of tACS during resting state and a visual
 detection task.
 The possibility to induce changes in EEG frequency power and to feed this
 data as a predictor into the fMRI analysis has not been realized so far.
 Changes induced by tACS can be investigated with high temporal and high
 spatial resolution, which could provide evidence for the physiological
 underpinnings of tACS.
 
\end_layout

\begin_layout Standard
The experiments have been successfully piloted.
 Currently we are working on adequate analysis strategies:
\end_layout

\begin_layout Standard
For the visual detection task:
\end_layout

\begin_layout Enumerate
Time-frequency analysis of EEG
\end_layout

\begin_layout Enumerate
Different EEG frequency bands as predictors for fMRI BOLD response.
\end_layout

\begin_layout Standard
For the resting state stimulation:
\end_layout

\begin_layout Enumerate
Time-frequency analysis of EEG
\end_layout

\begin_layout Enumerate
EEG microstates
\begin_inset CommandInset citation
LatexCommand citep
key "yuan2012spatiotemporal"

\end_inset


\end_layout

\begin_layout Enumerate
BOLD resting state networks
\end_layout

\begin_layout Standard
\begin_inset Newpage pagebreak
\end_inset


\end_layout

\begin_layout Standard
\begin_inset CommandInset bibtex
LatexCommand bibtex
bibfiles "/home/albert/Desktop/Lyxfiles/Doctoral_references3"
options "bibtotoc,humannat"

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Newpage pagebreak
\end_inset


\end_layout

\begin_layout Standard
\begin_inset CommandInset toc
LatexCommand tableofcontents

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Newpage pagebreak
\end_inset


\end_layout

\begin_layout Standard
\start_of_appendix

\end_layout

\end_body
\end_document
