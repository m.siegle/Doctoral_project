#LyX 2.2 created this file. For more info see http://www.lyx.org/
\lyxformat 508
\begin_document
\begin_header
\save_transient_properties true
\origin unavailable
\textclass article
\begin_preamble
\usepackage{tocbibind}

\usepackage {url}
%\renewcommand\usepackage [round,authoryear]{natbib}

\usepackage{scrpage2}
\usepackage{graphicx}
\usepackage{amssymb}
\usepackage{booktabs}
\usepackage{nomencl}
\usepackage{booktabs}
\usepackage{multirow}
\usepackage{textfit}
\usepackage{hyphenat}
\usepackage{float}
\usepackage{rotfloat}
\usepackage{floatflt}
\usepackage{rotating}
\usepackage{color}
%\usepackage[utf8]{inputenc}
%\usepackage[normalmargins,normallists]{savetrees} %package that reduces "white space"
\usepackage[footnotesize]{caption}
\usepackage{url}
\urlstyle{tt}
\usepackage[version=3]{mhchem}

\linespread{1.5}
\usepackage{acronym}
\usepackage{booktabs}
\usepackage[bookmarksopenlevel=section,colorlinks, linkcolor = black, citecolor = blue, filecolor = black, urlcolor = black]{hyperref}

\pagestyle{scrheadings} 
%\renewcommand\bibliographystyle{humannat}
\usepackage{tikz}
\end_preamble
\use_default_options true
\maintain_unincluded_children false
\language english
\language_package default
\inputencoding utf8
\fontencoding global
\font_roman "default" "default"
\font_sans "default" "default"
\font_typewriter "default" "default"
\font_math "auto" "auto"
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100 100
\font_tt_scale 100 100
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\float_placement H
\paperfontsize 12
\spacing onehalf
\use_hyperref false
\papersize default
\use_geometry true
\use_package amsmath 1
\use_package amssymb 1
\use_package cancel 1
\use_package esint 1
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 1
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine natbib
\cite_engine_type authoryear
\biblio_style abbrvnat
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date true
\justification true
\use_refstyle 1
\index Index
\shortcut idx
\color #008000
\end_index
\leftmargin 2.5cm
\topmargin 2cm
\rightmargin 2.5cm
\bottommargin 2cm
\secnumdepth 3
\tocdepth 4
\paragraph_separation indent
\paragraph_indentation default
\quotes_language english
\papercolumns 1
\papersides 1
\paperpagestyle headings
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
begin{titlepage}
\backslash
pagenumbering{Roman}
\end_layout

\begin_layout Plain Layout


\backslash
setcounter{page}{1}
\backslash
addcontentsline{toc}{section}{Frontpage}
\end_layout

\end_inset


\begin_inset space \hspace{}
\length 9cm
\end_inset


\begin_inset Newline newline
\end_inset

 
\begin_inset Graphics
	filename UMG.jpg
	lyxscale 10
	scale 25

\end_inset


\begin_inset space \hspace{}
\length 37page%
\end_inset


\begin_inset Graphics
	filename GGNB3.jpg
	lyxscale 10
	scale 3

\end_inset


\end_layout

\begin_layout Standard
\begin_inset CommandInset line
LatexCommand rule
offset "0.5ex"
width "100text%"
height "1pt"

\end_inset


\end_layout

\begin_layout Standard
\begin_inset VSpace 1.5cm
\end_inset


\end_layout

\begin_layout Standard
\align center
\begin_inset ERT
status open

\begin_layout Plain Layout

{
\end_layout

\end_inset


\series bold
\size larger

\begin_inset ERT
status open

\begin_layout Plain Layout

{
\end_layout

\end_inset

Second progress report
\begin_inset ERT
status open

\begin_layout Plain Layout

}
\end_layout

\end_inset

 
\series default
\size default

\begin_inset ERT
status open

\begin_layout Plain Layout

}
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\align center
\begin_inset ERT
status open

\begin_layout Plain Layout

{
\end_layout

\end_inset

- 18 months after start of thesis project -
\begin_inset ERT
status open

\begin_layout Plain Layout

}
\end_layout

\end_inset

 
\begin_inset VSpace 1.5cm
\end_inset


\end_layout

\begin_layout Standard
\align center
\begin_inset VSpace vfill
\end_inset


\end_layout

\begin_layout Standard
\paragraph_spacing onehalf
Name: Albert Lehr
\end_layout

\begin_layout Standard
\paragraph_spacing onehalf
GGNB program: Neurosciences
\end_layout

\begin_layout Standard
\paragraph_spacing onehalf
Date of progress report: 15.03.
 2018
\end_layout

\begin_layout Standard
\paragraph_spacing onehalf
Date of thesis advisory committee meeting: 19.03.
 2018
\end_layout

\begin_layout Standard
Supervisor: Prof.
 Andrea Antal
\end_layout

\begin_layout Standard
\paragraph_spacing onehalf
2nd and 3rd member of thesis advisory committee: 
\end_layout

\begin_layout Standard
\paragraph_spacing onehalf
\begin_inset space \qquad{}
\end_inset

Arezoo Pooresmaeili, Ph.D.
\end_layout

\begin_layout Standard
\paragraph_spacing onehalf
\begin_inset space \qquad{}
\end_inset

Prof.
 Susann Boretius 
\end_layout

\begin_layout Standard
\paragraph_spacing onehalf
Title of project: 
\end_layout

\begin_layout Standard
\begin_inset Quotes eld
\end_inset

Modulation of neuronal excitability in deep brain areas by electric stimulation
\begin_inset Quotes erd
\end_inset


\end_layout

\begin_layout Standard
\paragraph_spacing onehalf
Starting date of thesis: 01.10.
 2016 
\end_layout

\begin_layout Standard
\paragraph_spacing onehalf
Deadline for submission of thesis: 30.09.
 2019
\begin_inset Separator latexpar
\end_inset


\end_layout

\begin_layout Standard
\align center
\begin_inset VSpace vfill
\end_inset


\end_layout

\begin_layout Standard
Credits gathered so far:
\end_layout

\begin_layout Standard
\begin_inset space \qquad{}
\end_inset

- lectures, colloquia, seminars (min.
 5 C): 
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
hfill
\end_layout

\end_inset

 2 C 
\end_layout

\begin_layout Standard
\begin_inset space \qquad{}
\end_inset

- methods courses (min.
 2 C): 4.5 C
\end_layout

\begin_layout Standard
\begin_inset space \qquad{}
\end_inset

- teaching (min.
 4 C): 2.5 C
\end_layout

\begin_layout Standard
\begin_inset space \qquad{}
\end_inset

- scientific meetings (min.
 2 C): 2 C
\end_layout

\begin_layout Standard
\begin_inset space \qquad{}
\end_inset

- key qualifications (min.
 1 C): 7 C
\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
end{titlepage}
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Newpage pagebreak
\end_inset


\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
thispagestyle{empty}
\end_layout

\end_inset


\end_layout

\begin_layout Standard

\series bold
\size larger
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
null
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
vfill
\end_layout

\end_inset


\end_layout

\begin_layout Standard

\size large
First Referee: Professor Dr.
 Melanie Wilke
\end_layout

\begin_layout Standard

\size large
Second Referee: Professor Dr.
 Mathias Bähr
\begin_inset Newline newline
\end_inset


\end_layout

\begin_layout Standard

\size large
Date of submission of the Master's thesis: 31st of March, 2016
\end_layout

\begin_layout Standard

\size large
Date of written examination: 24th of August, 2015
\end_layout

\begin_layout Standard

\size large
Dates of oral examinations: 26th and 27th of August, 2015
\end_layout

\begin_layout Standard
\begin_inset Newpage pagebreak
\end_inset


\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
pagenumbering{Roman}
\end_layout

\begin_layout Plain Layout


\backslash
setcounter{page}{2}
\end_layout

\end_inset


\end_layout

\begin_layout Section*
Modulation of neural excitability in deep brain areas by electric stimulation
\begin_inset CommandInset label
LatexCommand label
name "sec:Modulation-of-neural"

\end_inset


\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
addcontentsline{toc}{section}{Report}
\end_layout

\end_inset


\end_layout

\begin_layout Standard
Transcranial electrical brain stimulation (tEBS) is a promising technique
 to induce changes in patients and healthy subjects while minimizing risks.
 It has been used increasingly to treat psychological and neurological diseases
 as depression and stroke, respectivly 
\begin_inset CommandInset citation
LatexCommand citep
key "Lefaucheur2017"

\end_inset

.
 However, mostly cortical areas have been the target of these stimulation
 protocols; the induction of changes in neural activity in deeper brain
 areas has still not been adequately adressed.
 
\end_layout

\begin_layout Standard
Transcranial alternating current stimulation (tACS) and transcranial random
 noise stimulation (tRNS) will be the two applied stimulation techniques.
 tACS was proven capable of entraining pyhsiological neural oscilations.
 Similary, pertubations of these osciallations allow to alter cognitive
 functions and thereby to investigate the functional significance of neural
 oscillations 
\begin_inset CommandInset citation
LatexCommand citep
key "Chander2016"

\end_inset

.
 Additionally to the application of a single frequency alternating current,
 frequencies can be nested which allows to entrain physiological frequency-coupl
ing as Theta-Gamma-coupling 
\begin_inset CommandInset citation
LatexCommand citep
key "Alekseichuk2016a"

\end_inset

.
\begin_inset Newline newline
\end_inset

tRNS is characterized by the application of alternating currents with frequcies
 that are randomly distributed over a definied frequency spectrum.
 Improvements in perception and arithmetic learning tasks have been detected
 by stochastic resonance, although the concrete mechanism of action still
 remains unidentified 
\begin_inset CommandInset citation
LatexCommand citep
key "Snowball2013,VanderGroen2016"

\end_inset

.
 Advantages of tRNS are the weak skin irritation and the absence of phosphene
 induction in participants.
 
\end_layout

\begin_layout Standard
The goal is to explore the possibility of applying low-intensity tEBS to
 change the activity of deeper brain structures.
 As it is easily applicable, non-invasive, affordable and painless, it proves
 an intriguing technique.
 Still, changing brain activity in deeper brain areas with relatively small
 induced electrical field sizes is a challenging task.
 
\end_layout

\begin_layout Standard
The Anterior Cingulate Cortex (ACC) is a deeper brain area involved in higher-le
vel functions as decision-making, and error detection and conflict monitoring
 but also the regulation autonomic functions as blood pressure and heart
 rate (wikipedia).
 Participants have to name the ink colour of words that are either congruent
 (the word 
\color green
Green
\color inherit
 written in green ink) or incongruent (
\color blue
Green
\color inherit
 written in blue ink), which induces a conflict and by this activates the
 dorsal ACC (dACC).
 The used Stroop task is a blocked-design task which involves blocks of
 congruent, incongruent, and neutral words (not colour words) of 10 minutes
 length with an earlier training phase.
 The pilot study for the task has been conducted, confirming results from
 the literature, especially the paper the design was based on 
\begin_inset CommandInset citation
LatexCommand citep
key "Andrews-Hanna2011"

\end_inset

.
 The accuracies and reaction times of the responses constitute the behavioral
 readout of the Stroop task.
 
\end_layout

\begin_layout Standard
Targeting of the dorsal Anterior Cingulate Cortex will be conducted with
 two seperate modelling softwares (HD target and SimNBIS) developed for
 predicting the current flow through the head during tEBS.
 
\end_layout

\begin_layout Standard
Transcranial alternating current (tACS) and random noise (tRNS) stimulation
 both constitute suitable stimulation paradigms for induncing changes in
 the dACC during the Stroop task.
 During the Stroop task an increase in theta band power ~ 400
\begin_inset space ~
\end_inset

ms after the stimulus has been observed with source analysis pointing to
 the dACC as the main oscillator 
\begin_inset CommandInset citation
LatexCommand citep
key "Hanslmayr2008"

\end_inset

.
 Thus, entrainment in theta power around 5
\begin_inset space ~
\end_inset

Hz could lead to changes in the activation.
 The activity of dACC was correlated with the increase of Stroop interference.
 By modulating the dACC activity changes in the Stroop interference can
 possibly be induced and thereby a causal link between the activity and
 the interfernce can possibly be established.
 
\begin_inset Newline newline
\end_inset

Changes in the Stroop task due to tRNS are subject to exploration, although
 application of tRNS in the Theta band range presumambly will lead to the
 same induced activity changes as Theta-tACS.
 Both stimulation paradigms will be applied throughout the length of the
 experiment.
 
\end_layout

\begin_layout Standard
Several electrode montages will be tested for their presumable effect on
 the dorsal anterior cingulate cortex (n
\begin_inset space ~
\end_inset

=
\begin_inset space ~
\end_inset

8).
 A double-blind crossover study design in which every participant undergoes
 stimulation and sham stimulation in different experimental sessions will
 be used.
 As intense stimulation is neede to reach deeper brain areas effectively,
 an approach combining several stimulation montages in a temporal manner
 is considered.
 Stimulation would sequentially switch from one montage to the next for
 a limited period of time around 10
\begin_inset space ~
\end_inset

s.
 This allows to not overstimulate the cortical brain areas above the dACC
 but still target it.
 
\end_layout

\begin_layout Standard
After robust cognitive/behavioral effects of the stimulation strategy have
 been established, the setup will be measured in the MRI scanner, thus adding
 a neurophysiological readout to the behavioural readout, which could be
 BOLD signal, functional network connectivity and or arterial spin labeling
 (n
\begin_inset space ~
\end_inset

=
\begin_inset space ~
\end_inset

20).
 As variability of the success of tES treatments is a great concern in the
 field, factors underlying variablity as cebrerospinal fluid and bone thickness
 will be measured and incoporated into the analysis of the effects of the
 stimulation.
 The MRI
\begin_inset space ~
\end_inset

-
\begin_inset space ~
\end_inset

Führerschein will be obtained.
\end_layout

\begin_layout Standard
In the future EEG measurements can be added to the measurement procedure
 to conduct source analyses and provide a clearer insight into the temporal
 aspect of the stimulation procedure.
 Stimulation within single trials of the Stroop task are possible as a way
 to adjust and reduce the overall period of stimulation without compromising
 its efficacy.
\end_layout

\begin_layout Standard
The ethics committee of the University Medical Center Göttingen has approved
 the ethics proposal under the application number 19/1/17.
\end_layout

\begin_layout Standard
So far I have obtained 3.5 ECTS points for my participation in multiple courses.
 A minimum of 4.5 ECTS points will be acquired next semester.
\end_layout

\begin_layout Standard
\begin_inset Newpage pagebreak
\end_inset


\end_layout

\begin_layout Standard
\begin_inset CommandInset bibtex
LatexCommand bibtex
bibfiles "/home/albert/Desktop/Lyxfiles/Doctoral_references"
options "bibtotoc,humannat"

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Newpage pagebreak
\end_inset


\end_layout

\begin_layout Standard
\begin_inset CommandInset toc
LatexCommand tableofcontents

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Newpage pagebreak
\end_inset


\end_layout

\begin_layout Standard
\start_of_appendix

\end_layout

\end_body
\end_document
