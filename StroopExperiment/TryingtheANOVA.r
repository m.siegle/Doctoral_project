# import the hallo.csv - file from ~/Doctoral_project/StroopExperiment/
require(car)
require(lme4)
require(plyr)




# load the Data in
STACS1_Day1 <- read.csv("~/Doctoral_project/StroopExperiment/1STACS_Day_1.csv")

#boxplot the data
boxplot(RT ~ TrialType * session, col = c("white","lightgray"), STACS1_Day1)


# put it into a mixed linear model: TrialType and session as fixed effects,
# participant as random effect ( for multiple measurements)
STACS1_Day1.model <- lmer(RT ~ TrialType + session + (1| participant) + (1 | LearningDay), data = STACS1_Day1)
summary(STACS1_Day1.model)



# Likelihood Ratio Test
#random intercept model

# test if session is significantly influencing the model
STACS1_Day1.null_LRT1 <- lmer(RT ~ TrialType + (1| participant) + (1 | LearningDay), data=STACS1_Day1, REML=FALSE)
STACS1_Day1.model_LRT <- lmer(RT ~ session + TrialType + (1| participant) + (1| LearningDay), data=STACS1_Day1, REML=FALSE)


anova(STACS1_Day1.null_LRT1, STACS1_Day1.model_LRT)

# test if TrialType is significantly influencing the model
STACS1_Day1.null_LRT2 <- lmer(RT ~ session + (1| participant), data=STACS1_Day1, REML=FALSE)

anova(STACS1_Day1.null_LRT2, STACS1_Day1.model_LRT)
coef(STACS1_Day1.model_LRT)

#Likelihood Ratio Test
# Test interaction between TrialType and session:
STACS1_Day1.model_independent <- lmer(RT ~ TrialType + session + (1| participant) +(1 | LearningDay), data=STACS1_Day1, REML=FALSE)
STACS1_Day1.model_interaction <- lmer(RT ~ TrialType * session + (1| participant) + (1 | LearningDay), data=STACS1_Day1, REML=FALSE)

anova(STACS1_Day1.model_interaction, STACS1_Day1.model_independent)

# random slope model

STACS1_Day1.model_LST_session <- lmer(RT ~ TrialType + session + (1 + session| participant) + (1 + session | LearningDay), data=STACS1_Day1, REML=FALSE)
STACS1_Day1.null_LST_session <- lmer(RT ~ TrialType + (1 + session| participant) + (1 + session | LearningDay), data=STACS1_Day1, REML=FALSE)

STACS1_Day1.model_LST_TT <- lmer(RT ~ TrialType + session + (1 + TrialType| participant)+ (1 + TrialType | LearningDay), data=STACS1_Day1, REML=FALSE)
STACS1_Day1.null_LST_TT <- lmer(RT ~ session + (1 + TrialType| participant) + (1 + TrialType | LearningDay), data=STACS1_Day1, REML=FALSE)


anova(STACS1_Day1.model_LST_session,STACS1_Day1.null_LST_session)
anova(STACS1_Day1.null_LST_TT, STACS1_Day1.model_LST_TT)


coef(STACS1_Day1.model_LST)




# Test single subjects

STACS1_Day1[STACS1_Day1$participant %in% '1STACS-2',]

boxplot(RT ~ TrialType *  session, col = c("white","lightgray"), STACS1_Day1[STACS1_Day1$participant %in% '1STACS-8',])


xmdl = lm(RT ~  session + TrialType, data=STACS1_Day1[STACS1_Day1$participant %in% '1STACS-4',])



summary(xmdl)
qqnorm(residuals(xmdl))
plot(fitted(xmdl),residuals(xmdl))


STACS1_Day1.null_LRT1 <- lm(RT ~ TrialType, data=STACS1_Day1[STACS1_Day1$participant %in% '1STACS-4',], REML=FALSE)
STACS1_Day1.model_LRT <- lm(RT ~ TrialType + session, data=STACS1_Day1[STACS1_Day1$participant %in% '1STACS-4',], REML=FALSE)

anova(STACS1_Day1.null_LRT1, STACS1_Day1.model_LRT)





T_Test_pairwise_Neutral <- pairwise.t.test(hallo$RT,hallo$TrialType, p.adj="holm")

T_Test_pairwise_Neutral
