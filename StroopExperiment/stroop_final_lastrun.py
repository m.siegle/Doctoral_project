#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
This experiment was created using PsychoPy2 Experiment Builder (v1.85.0),
    on Mai 18, 2017, at 08:53
If you publish work using this script please cite the PsychoPy publications:
    Peirce, JW (2007) PsychoPy - Psychophysics software in Python.
        Journal of Neuroscience Methods, 162(1-2), 8-13.
    Peirce, JW (2009) Generating stimuli for neuroscience using PsychoPy.
        Frontiers in Neuroinformatics, 2:10. doi: 10.3389/neuro.11.010.2008
"""

from __future__ import absolute_import, division

import psychopy
psychopy.useVersion('latest')

from psychopy import locale_setup, gui, visual, core, data, event, logging, sound
from psychopy.constants import (NOT_STARTED, STARTED, PLAYING, PAUSED,
                                STOPPED, FINISHED, PRESSED, RELEASED, FOREVER)
import numpy as np  # whole numpy lib is available, prepend 'np.'
from numpy import (sin, cos, tan, log, log10, pi, average,
                   sqrt, std, deg2rad, rad2deg, linspace, asarray)
from numpy.random import random, randint, normal, shuffle
import os  # handy system and path functions
import sys  # to get file system encoding

# Ensure that relative paths start from the same directory as this script
_thisDir = os.path.dirname(os.path.abspath(__file__)).decode(sys.getfilesystemencoding())
os.chdir(_thisDir)

# Store info about the experiment session
expName = 'Stroop'  # from the Builder filename that created this script
expInfo = {u'session': u'', u'participant': u''}
dlg = gui.DlgFromDict(dictionary=expInfo, title=expName)
if dlg.OK == False:
    core.quit()  # user pressed cancel
expInfo['date'] = data.getDateStr()  # add a simple timestamp
expInfo['expName'] = expName

# Data file name stem = absolute path + name; later add .psyexp, .csv, .log, etc
filename = _thisDir + os.sep + u'data' + os.sep + '%s_%s_%s' %(expInfo['participant'],expInfo['session'], expInfo['date'])

# An ExperimentHandler isn't essential but helps with data saving
thisExp = data.ExperimentHandler(name=expName, version='',
    extraInfo=expInfo, runtimeInfo=None,
    originPath=u'C:\\Dokumente und Einstellungen\\User\\Desktop\\MeineDaten\\StroopExperiment\\stroop_final.psyexp',
    savePickle=True, saveWideText=True,
    dataFileName=filename)
# save a log file for detail verbose info
logFile = logging.LogFile(filename+'.log', level=logging.WARNING)
logging.console.setLevel(logging.WARNING)  # this outputs to the screen, not a file

endExpNow = False  # flag for 'escape' or other condition => quit the exp

# Start Code - component code to be run before the window creation

# Setup the Window
win = visual.Window(
    size=(1920, 1200), fullscr=True, screen=0,
    allowGUI=False, allowStencil=False,
    monitor='testMonitor', color='black', colorSpace='rgb',
    blendMode='avg', useFBO=True,
    units='norm')
# store frame rate of monitor if we can measure it
expInfo['frameRate'] = win.getActualFrameRate()
if expInfo['frameRate'] != None:
    frameDur = 1.0 / round(expInfo['frameRate'])
else:
    frameDur = 1.0 / 60.0  # could not measure, so guess

# Initialize components for Routine "instructPractice"
instructPracticeClock = core.Clock()
import random
from random import randint
from random import shuffle
halfnumberoftraining=100

PracticeRando=[]
for i in range(0, 2*halfnumberoftraining):
    PracticeRando.append(0)


#defining the function to permuate the triads (could have been made by hand [1,2,3] [3,2,1]...
def permutate(seq):
    """permutate a sequence and return a list of the permutations"""
    if not seq:
         return [seq]  # is an empty sequence
    else:
         temp = []
         for k in range(len(seq)):
             part = seq[:k] + seq[k+1:]
             #print k, part  # test
             for m in permutate(part):
                 temp.append(seq[k:k+1] + m)
                 #print m, seq[k:k+1], temp  # test
         return temp

 #defining where the fixation blocks are
Blockreihenfolge=range(25)
numberoffixblocks=7 #wrote a formula to get it from 
for i in range(0,numberoffixblocks):
    j=i*4
    Blockreihenfolge[j]=0

#randomization of the triads
sequences=permutate([1,2,3])
sequenceorder=[0,1,2,3,4,5]
sequenceorder=random.sample(sequenceorder, len(sequenceorder))

for g in range(0,6):
    Blockreihenfolge[1+g*4]=sequences[sequenceorder[g]][0]
    Blockreihenfolge[2+g*4]=sequences[sequenceorder[g]][1]
    Blockreihenfolge[3+g*4]=sequences[sequenceorder[g]][2]

#print Blockreihenfolge

alltrialsinarow=range(12*(numberoffixblocks+(numberoffixblocks-1)*3)+numberoffixblocks)

def Umwandlungnachtrials(triallistprobe):
 for index, item in enumerate(triallistprobe):
     if (item == 0 or item ==4 or item==8): #CONGtrials
                     triallistprobe[index] = 0
     elif (item == 1 or item ==2 or item==3 or item==5 or item==6 or item==7): #Incongtrials
                     triallistprobe[index] = 1
     elif (item == 9 or item ==10 or item==11 or item==12 or item==13 or item==14): #neutral in all blocks
                     triallistprobe[index] = 2
     elif (item == 15 or item ==16 or item==17 or item==18 or item==19 or item==20): #neutral only in neutral 
                      triallistprobe[index] = 3
 return(triallistprobe)

def eliminatenegativepriming(triallistcheck):
    index=0
    negprimingpairs = [(5,1),(5,7),(3,1),(3,7),(1,3),(1,6),(2,3),(2,6),(6,2),(6,5),(7,2),(7,5)]
    for index in range (0,11):
     for (firstprime, secondprimed) in negprimingpairs:
      if triallistcheck[index]==firstprime and triallistcheck[index+1]==secondprimed:
       return False
    return True

def eliminateThreecolours(triallistcheck):
    index=0
    Threecolours=[]

    a=0
    b=3
    c=6
    d=9
    e=12
    f=15
    g=18




    sameanswers=[(a,b,c),(a,b,d),(a,b,e),(a,b,f), (a,b,g),(b,c,d),(b,c,e),(b,c,f),(b,c,g),(c,d,e),(c,d,f),(c,d,g),(d,e,f),(d,e,g),(e,f,g)]
    sameanswers2=[(a,c,d),(a,c,e),(a,c,f),(a,c,g),(a,d,e),(a,d,f),(a,d,g),(a,e,f),(a,e,g),(a,f,g)]
    sameanswers3=[(b,d,e),(b,d,f),(b,d,g),(b,e,f),(b,e,g),(b,f,g),(c,e,f),(c,e,g),(c,f,g),(d,f,g)]
    samsanswers= sameanswers+sameanswers2+sameanswers3
    for (x,y,z) in sameanswers: 
        permutated= permutate([x,y,z])
        for i in range(len(permutated)):
            Threecolours.append(permutated[i]) 
    for index in range (0,10):
     for [firstprime, secondprimed, thirdprimed] in Threecolours:
      if triallistcheck[index]==firstprime and triallistcheck[index+1]==secondprimed and triallistcheck[index+2]==thirdprimed:
       return False

    a=1
    b=4
    c=7
    d=10
    e=13
    f=16
    g=19
    sameanswers=[(a,b,c),(a,b,d),(a,b,e),(a,b,f), (a,b,g),(b,c,d),(b,c,e),(b,c,f),(b,c,g),(c,d,e),(c,d,f),(c,d,g),(d,e,f),(d,e,g),(e,f,g)]
    sameanswers2=[(a,c,d),(a,c,e),(a,c,f),(a,c,g),(a,d,e),(a,d,f),(a,d,g),(a,e,f),(a,e,g),(a,f,g)]
    sameanswers3=[(b,d,e),(b,d,f),(b,d,g),(b,e,f),(b,e,g),(b,f,g),(c,e,f),(c,e,g),(c,f,g),(d,f,g)]
    samsanswers= sameanswers+sameanswers2+sameanswers3
    for (x,y,z) in sameanswers: 
        permutated= permutate([x,y,z])
        for i in range(len(permutated)):
            Threecolours.append(permutated[i]) 
    for index in range (0,10):
     for [firstprime, secondprimed, thirdprimed] in Threecolours:
      if triallistcheck[index]==firstprime and triallistcheck[index+1]==secondprimed and triallistcheck[index+2]==thirdprimed:
       return False


    a=2
    b=5
    c=8
    d=11
    e=14
    f=17
    g=20

    sameanswers=[(a,b,c),(a,b,d),(a,b,e),(a,b,f), (a,b,g),(b,c,d),(b,c,e),(b,c,f),(b,c,g),(c,d,e),(c,d,f),(c,d,g),(d,e,f),(d,e,g),(e,f,g)]
    sameanswers2=[(a,c,d),(a,c,e),(a,c,f),(a,c,g),(a,d,e),(a,d,f),(a,d,g),(a,e,f),(a,e,g),(a,f,g)]
    sameanswers3=[(b,d,e),(b,d,f),(b,d,g),(b,e,f),(b,e,g),(b,f,g),(c,e,f),(c,e,g),(c,f,g),(d,f,g)]
    samsanswers= sameanswers+sameanswers2+sameanswers3
    for (x,y,z) in sameanswers: 
        permutated= permutate([x,y,z])
        for i in range(len(permutated)):
            Threecolours.append(permutated[i]) 
    for index in range (0,10):
     for [firstprime, secondprimed, thirdprimed] in Threecolours:
      if triallistcheck[index]==firstprime and triallistcheck[index+1]==secondprimed and triallistcheck[index+2]==thirdprimed:
       return False

    return True

def eliminateThreeconditions(triallistcheck):
    index=0
    Threeconditions=[]

    a=9
    b=10
    c=11
    d=12
    e=13
    f=14

    sameanswers=[(a,b,c),(a,b,d),(a,b,e),(a,b,f),(b,c,d),(b,c,e),(b,c,f),(c,d,e),(c,d,f),(d,e,f)]
    sameanswers2=[(a,c,d),(a,c,e),(a,c,f),(a,d,e),(a,d,f),(a,e,f),(b,d,e),(b,d,f),(b,e,f),(c,e,f)]
    samsanswers= sameanswers+sameanswers2
    for (x,y,z) in sameanswers: 
        permutated= permutate([x,y,z])
        for i in range(len(permutated)):
            Threeconditions.append(permutated[i]) 
    for index in range (0,10):
     for [firstprime, secondprimed, thirdprimed] in Threecolours:
      if triallistcheck[index]==firstprime and triallistcheck[index+1]==secondprimed and triallistcheck[index+2]==thirdprimed:
       return False
      elif triallistcheck[index]!=firstprime and triallistcheck[index+1]!=secondprimed and triallistcheck[index+2]!=thirdprimed:
       return False


    return True



def Conditioncheck(triallist):
 triallist=random.sample(triallist, len(triallist))
 bedingung=0;
 gg=0;
 triallistprobe=list(triallist)
 triallistprobe=Umwandlungnachtrials(triallistprobe)
 index= 0;

 while gg < 10:
     if triallistprobe[gg] == triallistprobe[gg+1] == triallistprobe[gg+2]:
         triallist=random.sample(triallist, len(triallist))
         triallistprobe=list(triallist)
         triallistprobe=Umwandlungnachtrials(triallistprobe)
         gg=0; 
     else:
         gg=gg+1
 
         if gg==10:
                    triallistcheck=list(triallist)
                    if eliminateThreecolours(triallistcheck)==True:
                          gg=10
                    else:
                          gg=0
                          triallist=random.sample(triallist, len(triallist))
                          triallistprobe=list(triallist)
                          triallistprobe=Umwandlungnachtrials(triallistprobe)

                    if eliminateThreeconditions(triallistcheck)==True:
                          gg=10
                    else:
                          gg=0
                          triallist=random.sample(triallist, len(triallist))
                          triallistprobe=list(triallist)
                          triallistprobe=Umwandlungnachtrials(triallistprobe)
   

                    if 1 in triallistcheck and 2 in triallistcheck and 3 in triallistcheck:
                      if eliminatenegativepriming(triallistcheck)==True:
                          gg=10
                      else:
                          gg=0
                          triallist=random.sample(triallist, len(triallist))
                          triallistprobe=list(triallist)
                          triallistprobe=Umwandlungnachtrials(triallistprobe)
                              
     return (triallist)
fixblockswritten = 0;

for index in range(0,25):
    singletrialsCONG= [9,10,11,12,13,14,0,0,4,4,8,8]
    singletrialsINCONG=[9,10,11,12,13,14,1,2,3,5,6,7]
    singletrialsNEUT=[9,10,11,12,13,14,15,16,17,18,19,20] 
    if Blockreihenfolge[index] == 0:
         for secondind in range(0,13):
             alltrialsinarow[secondind+index*12+fixblockswritten] = 21
             if secondind==12:
                alltrialsinarow[secondind+index*12+fixblockswritten] = 22
                fixblockswritten +=1 
    elif Blockreihenfolge[index] == 1: 
         singletrialsCONGshuff=Conditioncheck(singletrialsCONG)
         for secondind in range(0,12):
             alltrialsinarow[secondind+index*12+fixblockswritten] = singletrialsCONGshuff[secondind]

    elif Blockreihenfolge[index] == 2:
         singletrialsINCONGshuff=Conditioncheck(singletrialsINCONG)
         for secondind in range(0,12):
             alltrialsinarow[secondind+index*12+fixblockswritten] = singletrialsINCONGshuff[secondind]
    elif Blockreihenfolge[index] == 3:
         singletrialsNEUTshuff=Conditioncheck(singletrialsNEUT)
         for secondind in range(0,12):
             alltrialsinarow[secondind+index*12+fixblockswritten] = singletrialsNEUTshuff[secondind]

instr1 = visual.TextStim(win=win, name='instr1',
    text=u'Bitte dr\xfcck;\nLINKS bei roten Buchstaben\nUNTEN bei gr\xfcnen Buchstaben\nRECHTS bei blauen Buchstaben\n\nZuerst kommen ein paar \xdcbungsdurchg\xe4nge.\n\nDr\xfcck Enter um zu beginnen.',
    font='Arial',
    pos=[0, 0], height=0.1, wrapWidth=None, ori=0, 
    color='white', colorSpace='rgb', opacity=1,
    depth=-1.0);

# Initialize components for Routine "Fixation"
FixationClock = core.Clock()
Fixate = visual.TextStim(win=win, name='Fixate',
    text='default text',
    font='Arial',
    pos=[0, 0], height=0.155, wrapWidth=None, ori=0, 
    color=[1,1,1], colorSpace='rgb', opacity=1,
    depth=0.0);

# Initialize components for Routine "trial"
trialClock = core.Clock()
word = visual.TextStim(win=win, name='word',
    text='default text',
    font='Arial',
    pos=[0, 0], height=0.155, wrapWidth=None, ori=0, 
    color=1.0, colorSpace='rgb', opacity=1,
    depth=0.0);
worte = visual.TextStim(win=win, name='worte',
    text='default text',
    font='Arial',
    pos=[0, 0.15], height=0.155, wrapWidth=None, ori=0, 
    color='white', colorSpace='rgb', opacity=1,
    depth=-2.0);

# Initialize components for Routine "feedback"
feedbackClock = core.Clock()
#msg variable just needs some value at start
msg=''
Successfulltrials=[]
Successfulltrialsoverall=0
feedback_2 = visual.TextStim(win=win, name='feedback_2',
    text='default text',
    font='Arial',
    pos=[0, 0], height=0.155, wrapWidth=None, ori=0, 
    color=[1,1,1], colorSpace='rgb', opacity=1,
    depth=-1.0);

# Initialize components for Routine "instruct"
instructClock = core.Clock()
instrText = visual.TextStim(win=win, name='instrText',
    text=u'Nachfolgend beginnen die richtigen Durchg\xe4nge.\n\nLINKS bei roten Buchstaben\nUNTEN bei gr\xfcnen Buchstaben\nRECHTS bei blauen Buchstaben\n\nDr\xfcck Enter um zu beginnen.',
    font='Arial',
    pos=[0, 0], height=0.1, wrapWidth=None, ori=0, 
    color=[1, 1, 1], colorSpace='rgb', opacity=1,
    depth=0.0);

# Initialize components for Routine "Fixation"
FixationClock = core.Clock()
Fixate = visual.TextStim(win=win, name='Fixate',
    text='default text',
    font='Arial',
    pos=[0, 0], height=0.155, wrapWidth=None, ori=0, 
    color=[1,1,1], colorSpace='rgb', opacity=1,
    depth=0.0);

# Initialize components for Routine "trial"
trialClock = core.Clock()
word = visual.TextStim(win=win, name='word',
    text='default text',
    font='Arial',
    pos=[0, 0], height=0.155, wrapWidth=None, ori=0, 
    color=1.0, colorSpace='rgb', opacity=1,
    depth=0.0);
worte = visual.TextStim(win=win, name='worte',
    text='default text',
    font='Arial',
    pos=[0, 0.15], height=0.155, wrapWidth=None, ori=0, 
    color='white', colorSpace='rgb', opacity=1,
    depth=-2.0);

# Initialize components for Routine "thanks"
thanksClock = core.Clock()
thanksText = visual.TextStim(win=win, name='thanksText',
    text='Dies ist das Ende des Experiments.\n\n\nVielen Dank!',
    font='arial',
    pos=[0, 0], height=0.155, wrapWidth=None, ori=0, 
    color=[1, 1, 1], colorSpace='rgb', opacity=1,
    depth=0.0);

# Create some handy timers
globalClock = core.Clock()  # to track the time since experiment started
routineTimer = core.CountdownTimer()  # to track time remaining of each (non-slip) routine 

# ------Prepare to start Routine "instructPractice"-------
t = 0
instructPracticeClock.reset()  # clock
frameN = -1
continueRoutine = True
# update component parameters for each repeat

for i in range(0,halfnumberoftraining):
    PracticeRando[i]=randint(0,20)
    PracticeRando[i+halfnumberoftraining]=randint(0,20)
 

shuffle(PracticeRando)
current_trial_num_int = PracticeRando.pop()
current_trial_num= []
current_trial_num.append(current_trial_num_int)
ready1 = event.BuilderKeyResponse()
# keep track of which components have finished
instructPracticeComponents = [instr1, ready1]
for thisComponent in instructPracticeComponents:
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED

# -------Start Routine "instructPractice"-------
while continueRoutine:
    # get current time
    t = instructPracticeClock.getTime()
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    
    # *instr1* updates
    if t >= 0.0 and instr1.status == NOT_STARTED:
        # keep track of start time/frame for later
        instr1.tStart = t
        instr1.frameNStart = frameN  # exact frame index
        instr1.setAutoDraw(True)
    
    # *ready1* updates
    if t >= 0.0 and ready1.status == NOT_STARTED:
        # keep track of start time/frame for later
        ready1.tStart = t
        ready1.frameNStart = frameN  # exact frame index
        ready1.status = STARTED
        # keyboard checking is just starting
        event.clearEvents(eventType='keyboard')
    if ready1.status == STARTED:
        theseKeys = event.getKeys(keyList=['return', 'space'])
        
        # check for quit:
        if "escape" in theseKeys:
            endExpNow = True
        if len(theseKeys) > 0:  # at least one key was pressed
            # a response ends the routine
            continueRoutine = False
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in instructPracticeComponents:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # check for quit (the Esc key)
    if endExpNow or event.getKeys(keyList=["escape"]):
        core.quit()
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# -------Ending Routine "instructPractice"-------
for thisComponent in instructPracticeComponents:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)

# the Routine "instructPractice" was not non-slip safe, so reset the non-slip timer
routineTimer.reset()

# set up handler to look after randomisation of conditions etc
practice = data.TrialHandler(nReps=10000.0, method='sequential', 
    extraInfo=expInfo, originPath=-1,
    trialList=[None],
    seed=None, name='practice')
thisExp.addLoop(practice)  # add the loop to the experiment
thisPractice = practice.trialList[0]  # so we can initialise stimuli with some values
# abbreviate parameter names if possible (e.g. rgb = thisPractice.rgb)
if thisPractice != None:
    for paramName in thisPractice.keys():
        exec(paramName + '= thisPractice.' + paramName)

for thisPractice in practice:
    currentLoop = practice
    # abbreviate parameter names if possible (e.g. rgb = thisPractice.rgb)
    if thisPractice != None:
        for paramName in thisPractice.keys():
            exec(paramName + '= thisPractice.' + paramName)
    
    # set up handler to look after randomisation of conditions etc
    Wrongresponse = data.TrialHandler(nReps=1, method='random', 
        extraInfo=expInfo, originPath=-1,
        trialList=data.importConditions('StroopParameters_final.xls', selection=current_trial_num),
        seed=None, name='Wrongresponse')
    thisExp.addLoop(Wrongresponse)  # add the loop to the experiment
    thisWrongresponse = Wrongresponse.trialList[0]  # so we can initialise stimuli with some values
    # abbreviate parameter names if possible (e.g. rgb = thisWrongresponse.rgb)
    if thisWrongresponse != None:
        for paramName in thisWrongresponse.keys():
            exec(paramName + '= thisWrongresponse.' + paramName)
    
    for thisWrongresponse in Wrongresponse:
        currentLoop = Wrongresponse
        # abbreviate parameter names if possible (e.g. rgb = thisWrongresponse.rgb)
        if thisWrongresponse != None:
            for paramName in thisWrongresponse.keys():
                exec(paramName + '= thisWrongresponse.' + paramName)
        
        # ------Prepare to start Routine "Fixation"-------
        t = 0
        FixationClock.reset()  # clock
        frameN = -1
        continueRoutine = True
        routineTimer.add(0.500000)
        # update component parameters for each repeat
        Fixate.setText('+')
        # keep track of which components have finished
        FixationComponents = [Fixate]
        for thisComponent in FixationComponents:
            if hasattr(thisComponent, 'status'):
                thisComponent.status = NOT_STARTED
        
        # -------Start Routine "Fixation"-------
        while continueRoutine and routineTimer.getTime() > 0:
            # get current time
            t = FixationClock.getTime()
            frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
            # update/draw components on each frame
            
            # *Fixate* updates
            if t >= 0.0 and Fixate.status == NOT_STARTED:
                # keep track of start time/frame for later
                Fixate.tStart = t
                Fixate.frameNStart = frameN  # exact frame index
                Fixate.setAutoDraw(True)
            frameRemains = 0.0 + 0.5- win.monitorFramePeriod * 0.75  # most of one frame period left
            if Fixate.status == STARTED and t >= frameRemains:
                Fixate.setAutoDraw(False)
            
            # check if all components have finished
            if not continueRoutine:  # a component has requested a forced-end of Routine
                break
            continueRoutine = False  # will revert to True if at least one component still running
            for thisComponent in FixationComponents:
                if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                    continueRoutine = True
                    break  # at least one component has not yet finished
            
            # check for quit (the Esc key)
            if endExpNow or event.getKeys(keyList=["escape"]):
                core.quit()
            
            # refresh the screen
            if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
                win.flip()
        
        # -------Ending Routine "Fixation"-------
        for thisComponent in FixationComponents:
            if hasattr(thisComponent, "setAutoDraw"):
                thisComponent.setAutoDraw(False)
        
        # ------Prepare to start Routine "trial"-------
        t = 0
        trialClock.reset()  # clock
        frameN = -1
        continueRoutine = True
        routineTimer.add(1.500000)
        # update component parameters for each repeat
        word.setColor(letterColor, colorSpace='rgb')
        word.setText(Anzeigetext)
        resp = event.BuilderKeyResponse()
        worte.setText(signal
)
        # keep track of which components have finished
        trialComponents = [word, resp, worte]
        for thisComponent in trialComponents:
            if hasattr(thisComponent, 'status'):
                thisComponent.status = NOT_STARTED
        
        # -------Start Routine "trial"-------
        while continueRoutine and routineTimer.getTime() > 0:
            # get current time
            t = trialClock.getTime()
            frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
            # update/draw components on each frame
            
            # *word* updates
            if t >= 0 and word.status == NOT_STARTED:
                # keep track of start time/frame for later
                word.tStart = t
                word.frameNStart = frameN  # exact frame index
                word.setAutoDraw(True)
            frameRemains = 0 + 1.5- win.monitorFramePeriod * 0.75  # most of one frame period left
            if word.status == STARTED and t >= frameRemains:
                word.setAutoDraw(False)
            
            # *resp* updates
            if t >= 0 and resp.status == NOT_STARTED:
                # keep track of start time/frame for later
                resp.tStart = t
                resp.frameNStart = frameN  # exact frame index
                resp.status = STARTED
                # keyboard checking is just starting
                win.callOnFlip(resp.clock.reset)  # t=0 on next screen flip
                event.clearEvents(eventType='keyboard')
            frameRemains = 0 + 1.5- win.monitorFramePeriod * 0.75  # most of one frame period left
            if resp.status == STARTED and t >= frameRemains:
                resp.status = STOPPED
            if resp.status == STARTED:
                theseKeys = event.getKeys(keyList=['left', 'down', 'right'])
                
                # check for quit:
                if "escape" in theseKeys:
                    endExpNow = True
                if len(theseKeys) > 0:  # at least one key was pressed
                    if resp.keys == []:  # then this was the first keypress
                        resp.keys = theseKeys[0]  # just the first key pressed
                        resp.rt = resp.clock.getTime()
                        # was this 'correct'?
                        if (resp.keys == str(corrAns)) or (resp.keys == corrAns):
                            resp.corr = 1
                        else:
                            resp.corr = 0
            
            # *worte* updates
            if t >= 0.0 and worte.status == NOT_STARTED:
                # keep track of start time/frame for later
                worte.tStart = t
                worte.frameNStart = frameN  # exact frame index
                worte.setAutoDraw(True)
            frameRemains = 0.0 + 1.0- win.monitorFramePeriod * 0.75  # most of one frame period left
            if worte.status == STARTED and t >= frameRemains:
                worte.setAutoDraw(False)
            
            # check if all components have finished
            if not continueRoutine:  # a component has requested a forced-end of Routine
                break
            continueRoutine = False  # will revert to True if at least one component still running
            for thisComponent in trialComponents:
                if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                    continueRoutine = True
                    break  # at least one component has not yet finished
            
            # check for quit (the Esc key)
            if endExpNow or event.getKeys(keyList=["escape"]):
                core.quit()
            
            # refresh the screen
            if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
                win.flip()
        
        # -------Ending Routine "trial"-------
        for thisComponent in trialComponents:
            if hasattr(thisComponent, "setAutoDraw"):
                thisComponent.setAutoDraw(False)
        # check responses
        if resp.keys in ['', [], None]:  # No response was made
            resp.keys=None
            # was no response the correct answer?!
            if str(corrAns).lower() == 'none':
               resp.corr = 1  # correct non-response
            else:
               resp.corr = 0  # failed to respond (incorrectly)
        # store data for Wrongresponse (TrialHandler)
        Wrongresponse.addData('resp.keys',resp.keys)
        Wrongresponse.addData('resp.corr', resp.corr)
        if resp.keys != None:  # we had a response
            Wrongresponse.addData('resp.rt', resp.rt)
        
        # ------Prepare to start Routine "feedback"-------
        t = 0
        feedbackClock.reset()  # clock
        frameN = -1
        continueRoutine = True
        routineTimer.add(1.000000)
        # update component parameters for each repeat
        if resp.corr:#stored on last run routine
          msg="Korrekt"# RZ=%.3f" %(resp.rt)
        else:
          msg="Inkorrekt"
        feedback_2.setText(msg)
        # keep track of which components have finished
        feedbackComponents = [feedback_2]
        for thisComponent in feedbackComponents:
            if hasattr(thisComponent, 'status'):
                thisComponent.status = NOT_STARTED
        
        # -------Start Routine "feedback"-------
        while continueRoutine and routineTimer.getTime() > 0:
            # get current time
            t = feedbackClock.getTime()
            frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
            # update/draw components on each frame
            
            
            # *feedback_2* updates
            if t >= 0.0 and feedback_2.status == NOT_STARTED:
                # keep track of start time/frame for later
                feedback_2.tStart = t
                feedback_2.frameNStart = frameN  # exact frame index
                feedback_2.setAutoDraw(True)
            frameRemains = 0.0 + 1.0- win.monitorFramePeriod * 0.75  # most of one frame period left
            if feedback_2.status == STARTED and t >= frameRemains:
                feedback_2.setAutoDraw(False)
            
            # check if all components have finished
            if not continueRoutine:  # a component has requested a forced-end of Routine
                break
            continueRoutine = False  # will revert to True if at least one component still running
            for thisComponent in feedbackComponents:
                if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                    continueRoutine = True
                    break  # at least one component has not yet finished
            
            # check for quit (the Esc key)
            if endExpNow or event.getKeys(keyList=["escape"]):
                core.quit()
            
            # refresh the screen
            if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
                win.flip()
        
        # -------Ending Routine "feedback"-------
        for thisComponent in feedbackComponents:
            if hasattr(thisComponent, "setAutoDraw"):
                thisComponent.setAutoDraw(False)
        
        if resp.corr:#stored on last run routine
            Wrongresponse.finished=0
            current_trial_num_int = PracticeRando.pop()
            current_trial_num= []
            current_trial_num.append(current_trial_num_int)
            Successfulltrials.append(1)
            Successfulltrialsoverall+=1
            
        else:
            Wrongresponse.finished = 1
            practice.finished=0
            Successfulltrials.append(0)
        
        Probe = []
        Fertig = 0
        if len(Successfulltrials) > 20:
            for i in range(1,21):
                Probe.append(Successfulltrials[-i])
            if Probe.count(1)/ 20 >= 0.95:
                Fertig = 1
        
        
        if  Fertig==1 and Successfulltrialsoverall >= 50:
            Wrongresponse.finished=1
            practice.finished=1
        thisExp.nextEntry()
        
    # completed 1 repeats of 'Wrongresponse'
    
    # get names of stimulus parameters
    if Wrongresponse.trialList in ([], [None], None):
        params = []
    else:
        params = Wrongresponse.trialList[0].keys()
    # save data for this loop
    Wrongresponse.saveAsExcel(filename + '.xlsx', sheetName='Wrongresponse',
        stimOut=params,
        dataOut=['n','all_mean','all_std', 'all_raw'])
    thisExp.nextEntry()
    
# completed 10000.0 repeats of 'practice'

# get names of stimulus parameters
if practice.trialList in ([], [None], None):
    params = []
else:
    params = practice.trialList[0].keys()
# save data for this loop
practice.saveAsExcel(filename + '.xlsx', sheetName='practice',
    stimOut=params,
    dataOut=['n','all_mean','all_std', 'all_raw'])

# ------Prepare to start Routine "instruct"-------
t = 0
instructClock.reset()  # clock
frameN = -1
continueRoutine = True
# update component parameters for each repeat
ready = event.BuilderKeyResponse()
# keep track of which components have finished
instructComponents = [instrText, ready]
for thisComponent in instructComponents:
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED

# -------Start Routine "instruct"-------
while continueRoutine:
    # get current time
    t = instructClock.getTime()
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # *instrText* updates
    if t >= 0 and instrText.status == NOT_STARTED:
        # keep track of start time/frame for later
        instrText.tStart = t
        instrText.frameNStart = frameN  # exact frame index
        instrText.setAutoDraw(True)
    
    # *ready* updates
    if t >= 0 and ready.status == NOT_STARTED:
        # keep track of start time/frame for later
        ready.tStart = t
        ready.frameNStart = frameN  # exact frame index
        ready.status = STARTED
        # keyboard checking is just starting
        event.clearEvents(eventType='keyboard')
    if ready.status == STARTED:
        theseKeys = event.getKeys(keyList=['return'])
        
        # check for quit:
        if "escape" in theseKeys:
            endExpNow = True
        if len(theseKeys) > 0:  # at least one key was pressed
            # a response ends the routine
            continueRoutine = False
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in instructComponents:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # check for quit (the Esc key)
    if endExpNow or event.getKeys(keyList=["escape"]):
        core.quit()
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# -------Ending Routine "instruct"-------
for thisComponent in instructComponents:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)
# the Routine "instruct" was not non-slip safe, so reset the non-slip timer
routineTimer.reset()

# set up handler to look after randomisation of conditions etc
trials = data.TrialHandler(nReps=1.0, method='sequential', 
    extraInfo=expInfo, originPath=-1,
    trialList=data.importConditions('StroopParameters_final.xls', selection=alltrialsinarow),
    seed=None, name='trials')
thisExp.addLoop(trials)  # add the loop to the experiment
thisTrial = trials.trialList[0]  # so we can initialise stimuli with some values
# abbreviate parameter names if possible (e.g. rgb = thisTrial.rgb)
if thisTrial != None:
    for paramName in thisTrial.keys():
        exec(paramName + '= thisTrial.' + paramName)

for thisTrial in trials:
    currentLoop = trials
    # abbreviate parameter names if possible (e.g. rgb = thisTrial.rgb)
    if thisTrial != None:
        for paramName in thisTrial.keys():
            exec(paramName + '= thisTrial.' + paramName)
    
    # ------Prepare to start Routine "Fixation"-------
    t = 0
    FixationClock.reset()  # clock
    frameN = -1
    continueRoutine = True
    routineTimer.add(0.500000)
    # update component parameters for each repeat
    Fixate.setText('+')
    # keep track of which components have finished
    FixationComponents = [Fixate]
    for thisComponent in FixationComponents:
        if hasattr(thisComponent, 'status'):
            thisComponent.status = NOT_STARTED
    
    # -------Start Routine "Fixation"-------
    while continueRoutine and routineTimer.getTime() > 0:
        # get current time
        t = FixationClock.getTime()
        frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
        # update/draw components on each frame
        
        # *Fixate* updates
        if t >= 0.0 and Fixate.status == NOT_STARTED:
            # keep track of start time/frame for later
            Fixate.tStart = t
            Fixate.frameNStart = frameN  # exact frame index
            Fixate.setAutoDraw(True)
        frameRemains = 0.0 + 0.5- win.monitorFramePeriod * 0.75  # most of one frame period left
        if Fixate.status == STARTED and t >= frameRemains:
            Fixate.setAutoDraw(False)
        
        # check if all components have finished
        if not continueRoutine:  # a component has requested a forced-end of Routine
            break
        continueRoutine = False  # will revert to True if at least one component still running
        for thisComponent in FixationComponents:
            if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                continueRoutine = True
                break  # at least one component has not yet finished
        
        # check for quit (the Esc key)
        if endExpNow or event.getKeys(keyList=["escape"]):
            core.quit()
        
        # refresh the screen
        if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
            win.flip()
    
    # -------Ending Routine "Fixation"-------
    for thisComponent in FixationComponents:
        if hasattr(thisComponent, "setAutoDraw"):
            thisComponent.setAutoDraw(False)
    
    # ------Prepare to start Routine "trial"-------
    t = 0
    trialClock.reset()  # clock
    frameN = -1
    continueRoutine = True
    routineTimer.add(1.500000)
    # update component parameters for each repeat
    word.setColor(letterColor, colorSpace='rgb')
    word.setText(Anzeigetext)
    resp = event.BuilderKeyResponse()
    worte.setText(signal
)
    # keep track of which components have finished
    trialComponents = [word, resp, worte]
    for thisComponent in trialComponents:
        if hasattr(thisComponent, 'status'):
            thisComponent.status = NOT_STARTED
    
    # -------Start Routine "trial"-------
    while continueRoutine and routineTimer.getTime() > 0:
        # get current time
        t = trialClock.getTime()
        frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
        # update/draw components on each frame
        
        # *word* updates
        if t >= 0 and word.status == NOT_STARTED:
            # keep track of start time/frame for later
            word.tStart = t
            word.frameNStart = frameN  # exact frame index
            word.setAutoDraw(True)
        frameRemains = 0 + 1.5- win.monitorFramePeriod * 0.75  # most of one frame period left
        if word.status == STARTED and t >= frameRemains:
            word.setAutoDraw(False)
        
        # *resp* updates
        if t >= 0 and resp.status == NOT_STARTED:
            # keep track of start time/frame for later
            resp.tStart = t
            resp.frameNStart = frameN  # exact frame index
            resp.status = STARTED
            # keyboard checking is just starting
            win.callOnFlip(resp.clock.reset)  # t=0 on next screen flip
            event.clearEvents(eventType='keyboard')
        frameRemains = 0 + 1.5- win.monitorFramePeriod * 0.75  # most of one frame period left
        if resp.status == STARTED and t >= frameRemains:
            resp.status = STOPPED
        if resp.status == STARTED:
            theseKeys = event.getKeys(keyList=['left', 'down', 'right'])
            
            # check for quit:
            if "escape" in theseKeys:
                endExpNow = True
            if len(theseKeys) > 0:  # at least one key was pressed
                if resp.keys == []:  # then this was the first keypress
                    resp.keys = theseKeys[0]  # just the first key pressed
                    resp.rt = resp.clock.getTime()
                    # was this 'correct'?
                    if (resp.keys == str(corrAns)) or (resp.keys == corrAns):
                        resp.corr = 1
                    else:
                        resp.corr = 0
        
        # *worte* updates
        if t >= 0.0 and worte.status == NOT_STARTED:
            # keep track of start time/frame for later
            worte.tStart = t
            worte.frameNStart = frameN  # exact frame index
            worte.setAutoDraw(True)
        frameRemains = 0.0 + 1.0- win.monitorFramePeriod * 0.75  # most of one frame period left
        if worte.status == STARTED and t >= frameRemains:
            worte.setAutoDraw(False)
        
        # check if all components have finished
        if not continueRoutine:  # a component has requested a forced-end of Routine
            break
        continueRoutine = False  # will revert to True if at least one component still running
        for thisComponent in trialComponents:
            if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                continueRoutine = True
                break  # at least one component has not yet finished
        
        # check for quit (the Esc key)
        if endExpNow or event.getKeys(keyList=["escape"]):
            core.quit()
        
        # refresh the screen
        if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
            win.flip()
    
    # -------Ending Routine "trial"-------
    for thisComponent in trialComponents:
        if hasattr(thisComponent, "setAutoDraw"):
            thisComponent.setAutoDraw(False)
    # check responses
    if resp.keys in ['', [], None]:  # No response was made
        resp.keys=None
        # was no response the correct answer?!
        if str(corrAns).lower() == 'none':
           resp.corr = 1  # correct non-response
        else:
           resp.corr = 0  # failed to respond (incorrectly)
    # store data for trials (TrialHandler)
    trials.addData('resp.keys',resp.keys)
    trials.addData('resp.corr', resp.corr)
    if resp.keys != None:  # we had a response
        trials.addData('resp.rt', resp.rt)
    thisExp.nextEntry()
    
# completed 1.0 repeats of 'trials'

# get names of stimulus parameters
if trials.trialList in ([], [None], None):
    params = []
else:
    params = trials.trialList[0].keys()
# save data for this loop
trials.saveAsExcel(filename + '.xlsx', sheetName='trials',
    stimOut=params,
    dataOut=['n','all_mean','all_std', 'all_raw'])

# ------Prepare to start Routine "thanks"-------
t = 0
thanksClock.reset()  # clock
frameN = -1
continueRoutine = True
routineTimer.add(2.000000)
# update component parameters for each repeat
# keep track of which components have finished
thanksComponents = [thanksText]
for thisComponent in thanksComponents:
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED

# -------Start Routine "thanks"-------
while continueRoutine and routineTimer.getTime() > 0:
    # get current time
    t = thanksClock.getTime()
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # *thanksText* updates
    if t >= 0.0 and thanksText.status == NOT_STARTED:
        # keep track of start time/frame for later
        thanksText.tStart = t
        thanksText.frameNStart = frameN  # exact frame index
        thanksText.setAutoDraw(True)
    frameRemains = 0.0 + 2.0- win.monitorFramePeriod * 0.75  # most of one frame period left
    if thanksText.status == STARTED and t >= frameRemains:
        thanksText.setAutoDraw(False)
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in thanksComponents:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # check for quit (the Esc key)
    if endExpNow or event.getKeys(keyList=["escape"]):
        core.quit()
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# -------Ending Routine "thanks"-------
for thisComponent in thanksComponents:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)


# these shouldn't be strictly necessary (should auto-save)
thisExp.saveAsWideText(filename+'.csv')
thisExp.saveAsPickle(filename)
logging.flush()
# make sure everything is closed down
thisExp.abort()  # or data files will save again on exit
win.close()
core.quit()
