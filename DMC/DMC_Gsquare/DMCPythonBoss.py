# -*- coding: utf-8 -*-
from __future__ import division
# import for ScriptChooseTwoBest.py
import numpy as np
import scipy.stats.distributions as dist
from scipy.optimize import fmin
import os.path
import sys
import importlib

# import for the DMCPythonBoss

import subprocess
import shlex
import os
import time
import importlib
import imp
###########


moduleStudy = importlib.import_module('DefineStudy')
whichStudy = moduleStudy.whichStudy
print (whichStudy)

if __name__ == "__main__":
    a = int(sys.argv[1])

WhichMyConfig = sys.argv[1]
# imports data from different MyConfig
module = imp.load_source('MyConfig%s' %WhichMyConfig,'/scratch/lehr2/DMC/DMC_Gsquare/MyConfigs/MyConfigsof%s/MyConfig%s.py' %(whichStudy ,WhichMyConfig))
#module = importlib.import_module('MyConfig'+str(WhichMyConfig),package = None)
ID = module.ID
save_path = '/scratch/lehr2/DMC/%s/%s/' %(whichStudy, ID)


# In your script file
def getVarFromFile(filename):
    import imp
    f = open(filename)
   # global data
    data = imp.load_source('data', '', f)
    f.close()
    return data
###########
#defines how many times to run the first simplex
NrRuns = 30
if not os.path.exists('/scratch/lehr2/DMC/%s/' %whichStudy):
    os.makedirs('/scratch/lehr2/DMC/%s/' %whichStudy)

if not os.path.exists('/scratch/lehr2/DMC/%s/%s' %(whichStudy, ID)):
    os.makedirs('/scratch/lehr2/DMC/%s/%s' %(whichStudy, ID) )

def CheckFiles(Nr, ID, Type, whichStudy):
  a = 0
  if os.path.isfile('/scratch/lehr2/DMC/%s/%s/%s%s%s.txt' %(whichStudy, ID, ID, Type, str(Nr))):
    a = 1
  return a


# submits the first NrRuns jobs and checks if submission was successful
outputSIMPLEX = ['' for x in range(NrRuns)];
for NR in range(0,NrRuns):
  submitted = 0; 
  while (not ('is submitted' in outputSIMPLEX[NR])) and (not (CheckFiles(NR, ID,'SIMPLEX',whichStudy) == 1)):
    cmd = ['bsub -R scratch -q mpi -W 48:00 /usr/bin/python /scratch/lehr2/DMC/DMC_Gsquare/ScriptMakeFirstSimplex.py ' + str(NR) + ' ' + str(WhichMyConfig)]
    outputSIMPLEX[NR] = subprocess.check_output(cmd, shell =True)
    submitted = submitted + 1;
print('Successful submission of '  + str(submitted) + ' Jobs')

#check if all jobs are properly running or finished successfully
def CheckJobs(output, NrScript, Simplex, WhichMyConfig):
  output2 = ['' for x in range(NrScript)];
  for NR in range(0,NrScript):

    cmd = ['bjobs ' + output[NR][5:11]]
    output2[NR] = subprocess.check_output(cmd, shell = True)
    # if neither RUNNING nor FINISHED, restart the Job
    if  (not 'RUN' in output2[NR][90:110]) and  (not 'DONE' in output2[NR][90:110]) and (not 'PEND' in output2[NR][90:110]):
      if Simplex == 1:
         if (not CheckFiles(NR, ID,'SIMPLEX',whichStudy) == 1):
           cmd = ['bsub -R scratch -q mpi -W 48:00 /usr/bin/python /scratch/lehr2/DMC/DMC_Gsquare/ScriptMakeFirstSimplex.py ' + str(NR) + ' ' + str(WhichMyConfig)]
      else:
        if (not CheckFiles(NR, ID,'POLISHED',whichStudy) == 1):
          cmd = ['bsub -R scratch -q mpi-long -W 120:00 /usr/bin/python /scratch/lehr2/DMC/DMC_Gsquare/ScriptPolishBestParameters.py ' + str(NR) + ' '+ str(WhichMyConfig)]
      output[NR] = subprocess.check_output(cmd, shell =True)
  return output





# check periodically if n files exist
  # if yes, chooses two best and runs the polishing
notfinished =  [0 for x in range(NrRuns)]
while notfinished != [1 for x in range(0,NrRuns)]:
  time.sleep(5)
  print ('sleep done')	
  # checks if Jobs are running or restarts if there was a problem
  outputSIMPLEX = CheckJobs(outputSIMPLEX,NrRuns,1,WhichMyConfig)
  for index in range(NrRuns):
    if notfinished[index] == 0:
      notfinished[index] = CheckFiles(index, ID,'SIMPLEX',whichStudy)   
# end of while loop, just exists if all 30 files exist
print('Output files of ' + str(NrRuns) + ' Jobs successfully written')

#### chooses the two best parametersets
parameter = []
chi_square_statistic = []
#collect all data


for index in range(0,NrRuns):

  importID = save_path + ID + 'SIMPLEX' + str(index) + '.txt'
  imported = getVarFromFile(importID)
  parameter.append(list(imported.xopt))
  chi_square_statistic.append(imported.chi2stat)  

# just chhose the best two parameter sets
parameter = np.array(parameter)

chi_square_statistic = np.array(chi_square_statistic)
#get the 2 best parameter sets
best_paramset_1 = parameter[chi_square_statistic.argsort()[0]]
best_paramset_2 = parameter[chi_square_statistic.argsort()[1]]
best_paramset_statistic_1 = chi_square_statistic[chi_square_statistic.argsort()[0]]
best_paramset_statistic_2 = chi_square_statistic[chi_square_statistic.argsort()[1]]


completeName = os.path.join(save_path, '%sbestparam.txt' %ID )   
f = open(completeName,  'w')


f.write( 'best_paramset_1 = ' +  str(list(best_paramset_1)) + '\nbest_paramset_statistic_1 = ' +  str(best_paramset_statistic_1) + '\nbest_paramset_2 = ' +  str(list(best_paramset_2))+ '\nbest_paramset_statistic_2 = ' +  str(best_paramset_statistic_2) + '\nID = "{0}"'.format(ID))


f.close()
#############
notfinished = [0]
while notfinished != [1]:
  if os.path.isfile('/scratch/lehr2/DMC/%s/%s/%sbestparam.txt' %(whichStudy,ID,ID)):
     notfinished = [1]
print('2 Best Parameter-file written')


# submits the two polishing and checks if submission was successful
outputPOLISH = ['' for x in range(2)];
for NR in range(0,2):
  while not ('is submitted' in outputPOLISH[NR]) and (not (CheckFiles(NR, ID,'POLISHED',whichStudy) == 1)):
    cmd = ['bsub -R scratch -q mpi-long -W 120:00 /usr/bin/python /scratch/lehr2/DMC/DMC_Gsquare/ScriptPolishBestParameters.py ' + str(NR)+ ' ' + str(WhichMyConfig)]
    outputPOLISH[NR] = subprocess.check_output(cmd, shell =True)
print('2 Polishing Jobs submitted')



# checks periodically if 2 files exist
  # if yes, calculates the solution
notfinished =  [0 for x in range(0,2)]
while notfinished != [1 for x in range(2)]:
  time.sleep(10)
  outputPOLISH = CheckJobs(outputPOLISH,2,0,WhichMyConfig)
  for index in range(2):
    if notfinished[index] == 0:
      notfinished[index] = CheckFiles(index, ID, 'POLISHED',whichStudy)   
print('Polished parameter files written')



## chooses the best parameter set and gives it out:

importID = save_path + ID + 'POLISHED' + str(0) + '.txt'

imported = getVarFromFile(importID)
xopt_1 = imported.xopt

chi_sq_paramset_1 = imported.chi2stat

importID = save_path + ID + 'POLISHED' + str(1) + '.txt'
imported = getVarFromFile(importID)
xopt_2 = imported.xopt

chi_sq_paramset_2 = imported.chi2stat


summary_param = np.array([xopt_1,xopt_2])
summary_chi = np.array([chi_sq_paramset_1,chi_sq_paramset_2])
best_param = summary_param[np.argmin(summary_chi)]
best_chi = summary_chi[np.argmin(summary_chi)]






##make a txt file
completeName = os.path.join(save_path, '%sFINISHED.txt' %ID )   
f = open(completeName,  'w')


f.write( 'b_1 = ' +  str(best_param[0]) + '\nmu_c = ' +  str(best_param[1]) + '\nA = ' + str(best_param[2])  + '\nshape = ' + str(best_param[3]) + '\ntau = ' + str(best_param[4])  + '\nTer = ' + str(best_param[5]) + '\ns_r = ' + str(best_param[6])   +'\nGsquare = ' + str(best_chi) + '\nID = "{0}"'.format(ID) + '\nbest_statisic = ' + str(best_chi))


f.close()

##make a py file

completeName = os.path.join(save_path, '%sFINISHED.py' %ID )   
f = open(completeName,  'w')


f.write( 'b_1 = ' +  str(best_param[0]) + '\nmu_c = ' +  str(best_param[1]) + '\nA = ' + str(best_param[2])  + '\nshape = ' + str(best_param[3]) + '\ntau = ' + str(best_param[4])  + '\nTer = ' + str(best_param[5]) + '\ns_r = ' + str(best_param[6]) +'\nGsquare = ' + str(best_chi) + '\nID = "{0}"'.format(ID) + '\nbest_statisic = ' + str(best_chi))


f.close()

# make a , delimited file

completeName = os.path.join(save_path, '%sFINISHED_CSV.txt' %ID )   
f = open(completeName,  'w')


f.write(str(best_param[0]) + ',' +  str(best_param[1]) + ',' + str(best_param[2])  + ',' + str(best_param[3]) + ',' + str(best_param[4])  + ',' + str(best_param[5]) + ',' + str(best_param[6]) + ','+ str(best_chi))


f.close()


print('All Done')

### Move the files to the correct destination

#for NR in range(0,NrRuns):
#  cmd = ['mv /scratch/lehr2/DMC/DMC_Gsquare/out.' + outputSIMPLEX[NR][5:12] +' /scratch/lehr2/DMC/'+ ID ]
#  subprocess.call(cmd, shell = True)
#for NR in range(0,2): 
#  cmd = ['mv /scratch/lehr2/DMC/DMC_Gsquare/out.' + outputPOLISH[NR][5:12] +' /scratch/lehr2/DMC/'+ ID ]
#  subprocess.call(cmd, shell = True)


#print('Allmoved')
#finish
