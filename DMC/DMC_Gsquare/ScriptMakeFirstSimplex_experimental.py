# -*- coding: utf-8 -*-
from __future__ import division
import numpy as np
import scipy.stats.distributions as dist
from scipy.optimize import fmin
import os.path
import sys
import imp


import simplex

import importlib
###########
moduleStudy = importlib.import_module('DefineStudy')
whichStudy = moduleStudy.whichStudy

if __name__ == "__main__":
    a = int(sys.argv[2])

WhichMyConfig = sys.argv[2]
# catches the data, MyConfig has the ID of the participant, MyConfig is the only thing that needs updating to calculated new dataset
sys.path.append('scratch/lehr2/DMC/MyConfigs/MyConfigsof' + whichStudy)
#module = importlib.import_module('MyConfig'+str(WhichMyConfig),package = None)
module = imp.load_source('MyConfig%s' %WhichMyConfig,'/scratch/lehr2/DMC/DMC_Gsquare/MyConfigs/MyConfigsof%s/MyConfig%s.py' %(whichStudy ,WhichMyConfig))
ID = module.ID     
congruent_data = module.congruent_data
incongruent_data = module.incongruent_data


save_path = '/scratch/lehr2/DMC/%s/%s/' %(whichStudy, ID)

#data structure: 
#data[0]:     number of correct trials
#data[1]:     proportion correct
#data[2:7]:   .1, .3, .5, .7, .9 RT quantiles for correct trials (in ms)
#data[7]:     number of incorrect trials
#data[8]:     proportion of incorrect trials
#data[9:14]:  RT quantiles for incorrect trials (in ms). Because we have a number of errors > 10 in each congruency condition, .1, .3, .5 , .7, .9 RT quantiles are computed




# calls the script with a number, in order to see the number of runs that have been done
if __name__ == "__main__":
    a = int(sys.argv[1])

WhichCall = sys.argv[1]

ID = ID + 'SIMPLEX' + str(WhichCall)

observed_data = np.array([congruent_data, incongruent_data]) 

######### Changed by Albert Lehr
print ("has started")
from datetime import datetime
startTime = datetime.now()
#########

def DMC (compatibility, b_1,mu_c,A,shape, tau, mu_r, s_r, ntrials):
    #compatibilility: 1 if compatible, -1 if incompatible
    #b_1:upper decision bound (lower decision bound b_2 = -b_1)
    #mu_c:drift rate of the controlled process
    #A:peak amplitude of the automatic activation (= peak amplitude of the scaled gamma density function)
    #shape: shape of the automatic activation (= shape parameter of the scaled gamma density function)
    #tau:   computing [(shape-1)*tau] gives the peak latency of the automatic activation (= peak latency of the scaled gamma density function)
    #mu_r: mean nondecision time
    #s_r: variability in of duratio of nondecisional processes
    #s_z: variability in starting point
    #ntrials: nb of simulated trials (80,000 is good in general)
    
    #In the present simulation:
    #variability in starting point is uniformly distibuted with mean = 0 and range = s_z
    #variability in nondecision time is normally distributed with mean = mu_r and std = s_r
    
  

  
    dt = 1#integration constant (in milliseconds)
    sigma = 4#diffusion coefficient (I set sigma = 4, as in the simulations performed by Ulrich (see their Table 1))
    tmax = 1500# Maximum decision time duration (10 sc is arbitrary)
    
    t = np.linspace (dt, tmax, tmax/dt)
    A = A*compatibility#if the trial is compatible, the automatic activation will favor the correct response. If trial is incompatible, the automatic activation will favor the incorrect response
    
    #compute the drift rate of the superimposed process (see Ulrich p 169 equation 7 for the formula)
    mu = A*np.exp(-t/tau)* np.power(np.exp(1)*t/((shape-1)*tau), shape-1)*((shape-1)/t-1/tau)+ mu_c

    b_2 = -b_1 #incorrect response bound
    
    RT_correct = []#build a list to record response times of correct trials
    RT_error = []#build a list to record response times of incorrect trials
    
    for i in range (0,ntrials):#loop until all trials are terminated
        X = 0
        time = 0 #decision time
        for j in range (0, tmax):#loop until the trial (sample path) terminates
            time = time + dt
            dX = mu[j]*dt + sigma*np.sqrt(dt)*np.random.randn()
            X = X + dX
            if X >= b_1:#correct decision!
                residual = np.random.uniform(mu_r-s_r/2, mu_r+ s_r/2)
                RT_correct.append(time+residual)#save RT correct trial
                break #break the "for" loop line 49 --> end of the trial
            if X <= b_2:#incorrect decision!
                residual = np.random.uniform(mu_r-s_r/2, mu_r+ s_r/2)
                RT_error.append(time+residual)#save RT incorrect trial
                break
     
    RT_correct = np.array(RT_correct)#convert list into numpy array object (better to do some statistics)
    RT_error = np.array(RT_error)
    return RT_correct, RT_error
    
	
#note: if dt large (e.g., 5ms), remove dt/2. from decision times	



	
	######################
	###################### WARNING: this code assumes that there is at least one error observed in each experimental condition. If no error, modify the code to compute chi-square on correct trials. 
  
def gs (x):
    ncond=2 #number of conditions (compatible, incompatible)

   
    b_1 ,mu_c,A,shape, tau, mu_r, s_r = x #the current set of parameters chosen by Simplex
                      
    if b_1 < 0:
        b_1 = 1
    
    if mu_c < 0:
        mu_c = 0.01
    if A < 0:
        A = 1
    if shape < 0:
        shape = 0.1
    if tau < 0:
        tau = 1        
    if mu_r < 0:
        mu_r = 0        

    if s_r < 0:
        s_r = 0
        
    if s_r/2. > mu_r:
        s_r = mu_r*2-10 #minimum 5ms decision time        
                                    
    q_probs= np.array ([.1,.2,.2,.2,.2,.1])
    g_square = 0
    for g in range (ncond):          
        if(g==0):
          compatibility = 1.0     
        if(g==1):
          compatibility = -1.0
          
        #########  

        sim_TR_correct, sim_TR_incorrect = DMC (compatibility, b_1,mu_c,A,shape, tau, mu_r ,s_r,  5000)

        #########
            
        ntrials = len (sim_TR_correct) + len (sim_TR_incorrect)
        top_data = observed_data[g,1]*q_probs # observed proportion of trials between each RT quantile of correct responses 
        
                
        # proportion of simulated CORRECT trials for each bin bounded by observed RT quantiles (.1, .3, .5, .7, .9)  of correct responses
        pt1 = (len (sim_TR_correct [sim_TR_correct < observed_data[g, 2]])) /ntrials 
        pt2 = (len (sim_TR_correct [(sim_TR_correct < observed_data[g, 3]) & (sim_TR_correct >= observed_data[g, 2])])) /ntrials
        pt3 = (len (sim_TR_correct [(sim_TR_correct < observed_data[g, 4]) & (sim_TR_correct >= observed_data[g, 3])])) /ntrials 
        pt4 = (len (sim_TR_correct [(sim_TR_correct < observed_data[g, 5]) & (sim_TR_correct >= observed_data[g, 4])])) /ntrials
        pt5 = (len (sim_TR_correct [(sim_TR_correct < observed_data[g, 6]) & (sim_TR_correct >= observed_data[g, 5])])) /ntrials
        pt6 = (len (sim_TR_correct [sim_TR_correct >= observed_data[g, 6]])) /ntrials
     
        pred_p= np.array ([pt1,pt2,pt3,pt4,pt5,pt6])
        junk = pred_p
        for i in range (6):#replace 0 values by 0.0001 to avoid division by 0
            if junk[i] == 0:
                junk[i] = 0.0001
                
        top_g_array = top_data*np.log(top_data/junk)  
        top_g = np.sum (top_g_array) 
                
        #INCORRECT TRIALS
        if  observed_data[g, 7] == 0:#no error in the data!!!
            bot_g = 0
            
        elif observed_data[g, 7]>0 and observed_data[g, 7] <=5: #consider median RT only 
            b_probs= np.array([0.5,0.5])        
            bot_data = observed_data[g,8]*b_probs 
            pt1 = (len (sim_TR_incorrect[sim_TR_incorrect< observed_data[g, 11]])) /ntrials
            pt2 = (len (sim_TR_incorrect[sim_TR_incorrect >= observed_data[g, 11]])) /ntrials
            pred_p=np.array ([pt1,pt2])               
            junk = pred_p  
            for i in range (2):#replace 0 values by 0.0001 to avoid division by 0
                if junk[i] == 0:
                    junk[i] = 0.0001              
            bot_g_array = bot_data*np.log(bot_data/junk)
            bot_g= np.sum (bot_g_array) 
            
        elif observed_data[g, 7]>5 and observed_data[g, 7] <=10:#consider only .3 .5 .7 quantile
            b_probs=np.array([.3,.2,.2,.3])   #  .3, .5 ,.7 quantiles if 6-10 error
            bot_data = observed_data[g,8]*b_probs 
            pt1 = (len (sim_TR_incorrect[sim_TR_incorrect < observed_data[g, 10]])) /ntrials 
            pt2 = (len (sim_TR_incorrect[(sim_TR_incorrect < observed_data[g, 11]) & (sim_TR_incorrect >= observed_data[g, 10])])) /ntrials                
            pt3 = (len (sim_TR_incorrect[(sim_TR_incorrect < observed_data[g, 12]) & (sim_TR_incorrect >= observed_data[g, 11])])) /ntrials  
            pt4 = (len (sim_TR_incorrect[sim_TR_incorrect >= observed_data[g, 12]])) /ntrials 
            pred_p=np.array ([pt1,pt2,pt3,pt4])
            junk = pred_p  
            for i in range (4):#replace 0 values by 0.0001 to avoid division by 0
                if junk[i] == 0:
                    junk[i] = 0.0001              
            bot_g_array = bot_data*np.log(bot_data/junk)
            bot_g= np.sum (bot_g_array) 
            
        elif observed_data[g, 7]>10:
            b_probs = np.array ([.1,.2,.2,.2,.2,.1])   
            bot_data = observed_data[g,8]*b_probs 
            pt1 = (len (sim_TR_incorrect [sim_TR_incorrect < observed_data[g, 9]])) /ntrials 
            pt2 = (len (sim_TR_incorrect [(sim_TR_incorrect < observed_data[g, 10]) & (sim_TR_incorrect >= observed_data[g, 9])])) /ntrials
            pt3 = (len (sim_TR_incorrect [(sim_TR_incorrect < observed_data[g, 11]) & (sim_TR_incorrect >= observed_data[g, 10])])) /ntrials 
            pt4 = (len (sim_TR_incorrect [(sim_TR_incorrect < observed_data[g, 12]) & (sim_TR_incorrect >= observed_data[g, 11])])) /ntrials
            pt5 = (len (sim_TR_incorrect [(sim_TR_incorrect < observed_data[g, 13]) & (sim_TR_incorrect >= observed_data[g, 12])])) /ntrials            
            pt6 = (len (sim_TR_incorrect [sim_TR_incorrect >= observed_data[g, 13]])) /ntrials
            
            pred_p=np.array ([pt1,pt2,pt3,pt4,pt5,pt6])
            junk = pred_p
            for i in range (6):#replace 0 values by 0.0001 to avoid division by 0
                if junk[i] == 0:
                    junk[i] = 0.0001            
            bot_g_array = bot_data*np.log(bot_data/junk)
            bot_g= np.sum (bot_g_array) 
                 
        totg= (bot_g+top_g)*(observed_data[g, 0] + observed_data[g, 7])#weight by number of observed valid trials in the condition (observed_data[g, 0]) 
        g_square = g_square + totg
        
    g_square = 2* g_square 
    f = open(completeName,  'w')


    f.write('xopt = [' + str(b_1)+','+ str(mu_c)+','+str(A)+','+str(shape)+','+str(tau)+','+str(mu_r)+','+str(s_r)+']'+ '\nchi2stat = ' + str(g_square) + '\nID = "{0}"'.format(ID) + '\nstatus = 0')

    f.close()
    print (g_square)
    sys.stdout.flush()       
    return g_square


#############################
#####G-SQUARE OPTIMIZATION# 
#############################
routine = True
parameter = []
chi_square_statistic = []


completeName = os.path.join(save_path, '%s.txt' %ID )   

# this is DMC specific: a, mu_c, A, shape, tau, Ter
b_1=   np.round(np.random.uniform(35, 150)) # boundary seperation!
mu_c=  np.round(np.random.uniform(0.2,0.8), decimals = 1) # drift rate of controled process
A=     np.round(np.random.uniform(15,40)) # peak amplitude also known as weird greek symbol
shape= np.round(np.random.uniform(2.5,4.5), decimals = 1) # aka alpha
tau=   np.round(np.random.uniform(50,150)) # characteristic time
mu_r=  np.round(np.random.uniform(250,500)) # Ter
s_r =  np.round(np.random.uniform(30,130)) # variability of non-decision time



    
x0 = np.array ([b_1,mu_c,A,shape, tau, mu_r, s_r]) #initial guess for SIMPLEX

print ("starting point")
print (x0)

incr = [10, 0.1, 10,0.5, 10,30, 30]

mat = simplex.Simplex(gs, x0, increments = incr)
values, err, iter = mat.minimize(maxiters = 250)

#print (mat)
#print ('best_gsq')
#print (mat.currenterror)
#print ("best parameters")
#print (mat.guess)
#print ("errors")
#print (mat.errors)
#sys.stdout.flush()
#print ('simplex')
#print (mat.simplex)
print ('args = ', values)
print ('error = ', err)
print ('iterations = ', iter)           




#### changed by Albert Lehr 17/01/18

####


f = open(completeName,  'w')


f.write( 'xopt = ' + '['+str(mat.guess[0])+','+str(mat.guess[1])+','+str(mat.guess[2])+','+str(mat.guess[3])+','+str(mat.guess[4])+','+str(mat.guess[5])+','+str(mat.guess[6])+']' + '\nchi2stat = ' +  str(mat.currenterror) + '\nID = "{0}"'.format(ID) + '\nstatus = 1' )
#f.write('\n')
#f.write(str(datetime.now() - startTime));

f.close()


# best_param is array of a (Boundary seperation), mu_c (drift rate of controled process), A (peak amplitude), shape (alpha), tau (characteristic time for automatic activation gamma function), Ter (Nondecision time)



#The fitting time can be greatly improved by breaking the loop over the 20 different starting points. The idea is to create 20 scripts (and run them on 20 cores), each one featuring 1 starting point. 



