# -*- coding: utf-8 -*-
from __future__ import division
# import for ScriptChooseTwoBest.py
import numpy as np
import scipy.stats.distributions as dist
from scipy.optimize import fmin
import os.path
import sys
import importlib
from scipy.stats import gamma
# import for the DMCPythonBoss

import subprocess
import shlex
import os
import time
import importlib
import imp
###########
moduleStudy = importlib.import_module('DefineStudy')
whichStudy = moduleStudy.whichStudy
print (whichStudy)


def readout(lines,NR):
  read = str(lines[NR])
  read = read[2:-2]
  outread = read.translate(None,"'")
  
  return outread

# initialize lists to be exported

listofExperiment = []

if __name__ == "__main__":
    a = int(sys.argv[1])


WhichMyConfig = sys.argv[1]



for x in range(1,int(WhichMyConfig)):
# imports data from different MyConfig
  whichStudy = 'SDLPFC'
  module = imp.load_source('MyConfig%s' %WhichMyConfig,'/scratch/lehr2/DMC/DMC_Gsquare/MyConfigs/MyConfigsof%s/MyConfig%s.py' %(whichStudy ,str(x)))
  whichStudy = 'sdlpfc'
  #module = importlib.import_module('MyConfig'+str(x),package = None)
  ID = module.ID
  save_path = module.save_path
  save_path = '/scratch/lehr2/DMC/%s/%s/' %(whichStudy, ID)
  importfile = save_path + ID + 'FINISHED.txt'
  #moduleData = importlib.import_module()



  with open(importfile) as file:
      lines = []
      for line in file:
          # The rstrip method gets rid of the "\n" at the end of each line
          lines.append(line.rstrip().split(","))


  for i in range(0,len(lines)):
    exec(lines[i][0])


  t90 = gamma.ppf(.90, shape, scale = tau)
  tmax = tau * (shape - 1)

  if 'verum' in ID: 
    listofExperiment.append([ID] + [b_1] + [mu_c] + [A] + [shape] + [tau] + [Ter]+ [s_r] +[Gsquare] +[t90] + [tmax]  + ['verum'])
  elif 'sham' in ID:
    listofExperiment.append([ID] + [b_1] + [mu_c] + [A] + [shape] + [tau] + [Ter]+ [s_r] +[Gsquare] +[t90] + [tmax]  + ['sham'])
  else:
    print('there has been an error - could not find if session was verum or sham')




MyData = [['ID','Boundary','ControlledDriftRate','PeakAmplitude','Shape','Tau','NonDecisionTime','VariabilityofNondecisionTime','Gscore','t90','tmax', 'ShamOrVerum']] + listofExperiment 

SaveName = whichStudy + 'FINISHED.csv'
import csv
with open(SaveName, "wb") as f:
    writer = csv.writer(f)
    writer.writerows(MyData)
output = 'saved as ' + SaveName
print (output)



