# -*- coding: utf-8 -*-
from __future__ import division
import numpy as np
import scipy.stats.distributions as dist
from scipy.optimize import fmin
import os.path
import sys
import importlib

# catches the data, MyConfig has the ID of the participant, MyConfig is the only thing that needs updating to calculated new dataset
#in this script, ID/save_path and parameters are important
from MyConfig import *


     
# In your script file
def getVarFromFile(filename):
    import imp
    f = open(filename)
   # global data
    data = imp.load_source('data', '', f)
    f.close()
    return data






#data structure: 
#data[0]:     number of correct trials
#data[1]:     proportion correct
#data[2:7]:   .1, .3, .5, .7, .9 RT quantiles for correct trials (in ms)
#data[7]:     number of incorrect trials
#data[8]:     proportion of incorrect trials
#data[9:14]:  RT quantiles for incorrect trials (in ms). Because we have a number of errors > 10 in each congruency condition, .1, .3, .5 , .7, .9 RT quantiles are computed




# calls the script with a number, in order to see the number of runs that have been done
# this script has to be called with 0 or 1 in order to calculate either SIMPLEX
if __name__ == "__main__":
    a = int(sys.argv[1])

WhichCall = sys.argv[1]

#import the chi_square_statistic and parameter for every single of the 20 runs:

parameter = []
chi_square_statistic = []

#collect the best data


importID =  save_path + ID + 'bestparam' + '.txt'  
imported = getVarFromFile(importID)


# prepare data for the polishing


if int(WhichCall) == 0:
  best_paramset = imported.best_paramset_1
elif int(WhichCall) == 1:
  best_paramset = imported.best_paramset_2

# saving ID, two different files have to be saved in two runs of this script
ID = ID + 'POLISHED' + str(WhichCall)


observed_data = np.array([congruent_data, incongruent_data]) 

######### Changed by Albert Lehr
print ("has started")
from datetime import datetime
startTime = datetime.now()
#########

def DMC (compatibility, a,mu_c,A,shape, tau, Ter, ntrials):
    #compatibilility: 1 if compatible, -1 if incompatible
    #a       boundary separation
    #Ter     nondecision time  (in milliseconds)
    #mu_c    drift rate of the controlled process
    #A:      peak amplitude parameter of the gamma function
    #shape:  shape parameter of the gamma function
    #tau:    characteristic time parameter of the gamma function
   
    dt = 1   #integration constant (in milliseconds)
    sigma = 4  #diffusion coefficient
    tmax = 10000 #max decision time (inmilliseconds) (arbitrary)
    
    t = np.linspace (dt, tmax, tmax/dt) # counts from dt to tmax in equal steps
    A = A*compatibility#If trial is incompatible, the automatic activation favors the incorrect response
    
    #compute the drift rate of the superimposed process
    mu = A*np.exp(-t/tau)* np.power(np.exp(1)*t/((shape-1)*tau), shape-1)*((shape-1)/t-1/tau)+ mu_c
     
    RTcorrect = []
    RTerror = []
    
    for i in range (0,ntrials):#loop until all trials are terminated
        X = a/2. #unbiaised starting point for the decision process
        time = 0 #decision time
        
        for j in range (0, tmax):#loop until the decision process terminates
            time = time + dt
            dX = mu[j]*dt + sigma*np.sqrt(dt)*np.random.randn()
            X = X + dX
            if X >= a:#correct decision!
                RTcorrect.append(time+Ter)#add Ter to decision time
                break 
            if X <= 0:#incorrect decision!
                RTerror.append(time+Ter)
                break
     
    RTcorrect = np.array(RTcorrect)
    RTerror = np.array(RTerror)
    
    return RTcorrect, RTerror
    
	
#note: if dt large (e.g., 5ms), remove dt/2. from decision times	



	
	######################
	###################### WARNING: this code assumes that there is at least one error observed in each experimental condition. If no error, modify the code to compute chi-square on correct trials. 
  
def chis (x):
    ncond=2 #number of conditions (congruent, incongruent) 
    a, mu_c, A, shape, tau, Ter = x
                       
    q_probs= np.array ([.1,.2,.2,.2,.2,.1])
    chi_square = 0
    for g in range (ncond):          
        if(g==0):
          compatibility = 1.0     
        if(g==1):
          compatibility = -1.0
        


        #### changed by Albert Lehr: deleted 10E3 off both fiting routines:
        if routine == True:
            sim_TR_correct, sim_TR_incorrect = DMC (compatibility, a, mu_c, A, shape, tau, Ter, 10000) # 10000 stimulated trials per condition for first Simplex searches
        if routine == False:
            sim_TR_correct, sim_TR_incorrect = DMC (compatibility, a, mu_c, A, shape, tau, Ter, 50000)#50000 simulated trials per condition for additional Simplex searches (to polish optimization)  
        
        ######### 
        ntrials = len (sim_TR_correct) + len (sim_TR_incorrect)
        
        top_data = observed_data[g,1]*q_probs 
        pt1 = (len (sim_TR_correct [sim_TR_correct < observed_data[g, 2]])) /ntrials # calculates the ratio of simulated trials that are faster than the RT quantiles of the currently examined condition ( depends on g)
        pt2 = (len (sim_TR_correct [(sim_TR_correct < observed_data[g, 3]) & (sim_TR_correct >= observed_data[g, 2])])) /ntrials
        pt3 = (len (sim_TR_correct [(sim_TR_correct < observed_data[g, 4]) & (sim_TR_correct >= observed_data[g, 3])])) /ntrials 
        pt4 = (len (sim_TR_correct [(sim_TR_correct < observed_data[g, 5]) & (sim_TR_correct >= observed_data[g, 4])])) /ntrials
        pt5 = (len (sim_TR_correct [(sim_TR_correct < observed_data[g, 6]) & (sim_TR_correct >= observed_data[g, 5])])) /ntrials
        pt6 = (len (sim_TR_correct [sim_TR_correct >= observed_data[g, 6]])) /ntrials  # here ends with  >= 6, because observed_data[g,6] is the 0.9 quantile
                   
        pred_p= np.array ([pt1,pt2,pt3,pt4,pt5,pt6])
        
        junk = pred_p
        for i in range (6):#replace 0 values by 0.0001 (arbitrary) to avoid division by 0 in the chi-square computation
            if junk[i] == 0:
                junk[i] = 0.0001
        #compute chi-square for correct trials        
        top_chi_array = (np.square (top_data-pred_p))/junk
        top_chi = np.sum (top_chi_array) 
        
        #now compute chi-square for incorrect trials. Remember our adaptive procedure for computing RT quantiles of errors (see text)        
            
        if observed_data[g, 7]>0 and observed_data[g, 7] <=5: #consider median RT for errors 
            b_probs= np.array([0.5,0.5])        
            bot_data = observed_data[g,8]*b_probs # observed proportion of trials in each bin bounded by RT quantiles [incorrect trials]
            
            # predicted proportion of trials in each bin bounded by RT quantiles [incorrect trials]
            pt1 = (len (sim_TR_incorrect[sim_TR_incorrect< observed_data[g, 11]])) /ntrials
            pt2 = (len (sim_TR_incorrect[sim_TR_incorrect >= observed_data[g, 11]])) /ntrials
            pred_p=np.array ([pt1,pt2])               
            junk=pred_p  
            for i in range (2):#replace 0 values by 0.0001 (arbitrary) to avoid division by 0 in the chi-square computation
                if junk[i] == 0:
                    junk[i] = 0.0001              
            bot_chi_array = (np.square (bot_data-pred_p)) /junk
            bot_chi= np.sum (bot_chi_array) 
            
        if observed_data[g, 7]>5 and observed_data[g, 7] <=10:#consider .3 .5 .7 RT quantiles for errors
            b_probs=np.array([.3,.2,.2,.3])
            bot_data = observed_data[g,8]*b_probs 
            pt1 = (len (sim_TR_incorrect[sim_TR_incorrect < observed_data[g, 10]])) /ntrials 
            pt2 = (len (sim_TR_incorrect[(sim_TR_incorrect < observed_data[g, 11]) & (sim_TR_incorrect >= observed_data[g, 10])])) /ntrials                
            pt3 = (len (sim_TR_incorrect[(sim_TR_incorrect < observed_data[g, 12]) & (sim_TR_incorrect >= observed_data[g, 11])])) /ntrials  
            pt4 = (len (sim_TR_incorrect[sim_TR_incorrect >= observed_data[g, 12]])) /ntrials 
            pred_p=np.array ([pt1,pt2,pt3,pt4])
            junk = pred_p  
            for i in range (4):#replace 0 values by 0.0001 to avoid division by 0
                if junk[i] == 0:
                    junk[i] = 0.0001              
            bot_chi_array = (np.square(bot_data-pred_p)) /junk
            bot_chi= np.sum (bot_chi_array)   
            
        if observed_data[g, 7]>10:#consider .1 .3 .5 .7 .9 RT quantiles for errors
            b_probs = np.array ([.1,.2,.2,.2,.2,.1])   
            bot_data = observed_data[g,8]*b_probs 
            pt1 = (len (sim_TR_incorrect [sim_TR_incorrect < observed_data[g, 9]])) /ntrials 
            pt2 = (len (sim_TR_incorrect [(sim_TR_incorrect < observed_data[g, 10]) & (sim_TR_incorrect >= observed_data[g, 9])])) /ntrials
            pt3 = (len (sim_TR_incorrect [(sim_TR_incorrect < observed_data[g, 11]) & (sim_TR_incorrect >= observed_data[g, 10])])) /ntrials 
            pt4 = (len (sim_TR_incorrect [(sim_TR_incorrect < observed_data[g, 12]) & (sim_TR_incorrect >= observed_data[g, 11])])) /ntrials
            pt5 = (len (sim_TR_incorrect [(sim_TR_incorrect < observed_data[g, 13]) & (sim_TR_incorrect >= observed_data[g, 12])])) /ntrials            
            pt6 = (len (sim_TR_incorrect [sim_TR_incorrect >= observed_data[g, 13]])) /ntrials
            pred_p=np.array ([pt1,pt2,pt3,pt4,pt5,pt6])
            junk = pred_p
            for i in range (6):#replace 0 values by 0.0001 to avoid division by 0
                if junk[i] == 0:
                    junk[i] = 0.0001            
            bot_chi_array = (np.square (bot_data-pred_p)) /junk
            bot_chi= np.sum (bot_chi_array)
        


        totchi= (bot_chi+top_chi)*(observed_data[g, 0] + observed_data[g, 7])#weight by number of observed correct and incorrect trials in the condition
        
        chi_square = chi_square + totchi
        
    return chi_square


#############################
#####CHI-SQUARE OPTIMIZATION# 
#############################



######
#Change by Albert Lehr: range (2) instead of 20
######



#polish the 2 best parameter sets (from different starting points) by running additional SIMPLEX searches with more trials per condition
#and polish them

routine = False

xopt = fmin (chis, best_paramset)
chi_sq_paramset = chis(xopt)


#### changed by Albert Lehr

####


completeName = os.path.join(save_path, '%s.txt' %ID   )   
f = open(completeName,  'w')


f.write( 'xopt = ' +  str(list(xopt)) + '\nchi_sq_paramset = ' +  str(chi_sq_paramset) + '\nID = "{0}"'.format(ID))


f.close()



# best_param is array of a (Boundary seperation), mu_c (drift rate of controled process), A (peak amplitude), shape (alpha), tau (characteristic time for automatic activation gamma function), Ter (Nondecision time)



#The fitting time can be greatly improved by breaking the loop over the 20 different starting points. The idea is to create 20 scripts (and run them on 20 cores), each one featuring 1 starting point. 



